/**
 * This Module has common methods used accross fifferent azure function
 */
module.exports = {
  /**
 * Validate Http Request Parameters
 * @param HttpRequestObject HttpRequest Object
 * @return boolean true or false
 * IF request is valid return true
 */
  validateUsersAuthenticity: async function (req) {
    return new Promise(async (resolve, reject) => {
      const requestValidation = { ValidationStatus: false, ResponseMessage: 'Invalid Request', ResponseCode: 400, UserID: null, CompanyID: null };
      try{
        if(req.headers.authorization && req.headers.authorization.trim() !== '') {
          const authenticationHeader = req.headers.authorization.split('Token ');
          let authToken = '';
          if(authenticationHeader && authenticationHeader.length > 0) {
            authToken = authenticationHeader[1];
            const userData = await require('./clicktime_api').getUserInfoByAuthToken(authToken);
            if(userData) {
              requestValidation.ValidationStatus = true;
              requestValidation.ResponseMessage = 'Valid Request';
              requestValidation.ResponseCode = 200;
              requestValidation.UserID = userData.ID;
              requestValidation.UserEmail = userData.Email;
              requestValidation.UserName = userData.Name;
            } else{
              requestValidation.ValidationStatus = false;
              requestValidation.ResponseMessage = 'Invalid Authentication Token';
              requestValidation.ResponseCode = 401;
            }
          } else{
            requestValidation.ValidationStatus = false;
            requestValidation.ResponseMessage = 'Invalid Authentication Token';
            requestValidation.ResponseCode = 401;
          }
        } else{
          requestValidation.ValidationStatus = false;
          requestValidation.ResponseMessage = 'Invalid Authentication Token';
          requestValidation.ResponseCode = 401;
        }
        resolve(requestValidation);
      } catch(ex) {
        reject(requestValidation);
      }
    });
  },
  getResponseObject: function ({ status = 200, body }) {
    const responseObject = {
      headers: {
        'Content-Type': 'application/json'
      },
      status: status,
      body: body
    };
    return responseObject;
  },
  isNullOrEmpty: function (str) {
    return (!str || /^\s*$/.test(str));
  },
  tryParseJSON: function (jsonString) {
    try {
      var o = JSON.parse(jsonString);
      // Handle non-exception-throwing cases:
      // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
      // but... JSON.parse(null) returns null, and typeof null === "object",
      // so we must check for that, too. Thankfully, null is falsey, so this suffices:
      if (o) {
        return o;
      }
    } catch (e) { }
    return false;
  }
};
