// Import getResponseObject method from general utilities module
const getResponseObject = require('../general_utilities').getResponseObject;
module.exports = async function (context, req) {
  const requestValidation = await validateRequest(req);
  if (requestValidation.ValidationStatus) {
    let importJobDetailSummary = {};
    let errorMessage = '';
    await require('../DatabaseOperations/general_operations').getEntityTypeIDByImportJobID(req.params.id).then(async entityTypeID => {
      if(entityTypeID !== 0) {
        importJobDetailSummary = await require('../DatabaseOperations/general_operations').getImportJobDetailSummary({
          importJobID: req.params.id,
          userID: requestValidation.UserID,
          entityTypeID: entityTypeID
        }).catch(error => {
          errorMessage = error;
        });
      }else{
        errorMessage = 'Invalid Job ID';
      }
      context.res = getResponseObject({
        status: errorMessage.length === 0 ? 200 : 400,
        body: errorMessage.length === 0 ? JSON.stringify(importJobDetailSummary) : errorMessage
      });
    });
  } else {
    context.res = getResponseObject({
      status: requestValidation.ResponseCode,
      body: requestValidation.ResponseMessage
    });
  }
};

/**
 * Validate Http Request Parameters
 * @param HttpRequestObject HttpRequest Object
 * @return boolean true or false
 * IF request is valid return true
 */
async function validateRequest (req) {
  let requestValidation = { ValidationStatus: false, ResponseMessage: 'Invalid Request', ResponseCode: 400, UserID: null, CompanyID: null };
  try{
    if(req.params.id) {
      await require('../general_utilities').validateUsersAuthenticity(req).then(async userAuthenticityStatus => {
        requestValidation = userAuthenticityStatus;
      });
    } else{
      requestValidation.ValidationStatus = false;
      requestValidation.ResponseMessage = 'Missing parameter id';
      requestValidation.ResponseCode = 400;
    }
  } catch(ex) {
    requestValidation.ResponseMessage = ex.message;
  }
  return requestValidation;
}
