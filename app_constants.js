module.exports = {
  /**
    * ClickTime API Properties
    */
  ClickTimeAPI: {
    Protocol: 'https://',
    Host: 'api.clicktime.com',
    /** List of ClickTime API Endpoints */
    Endpoints: {
      GetTimeReport: '/v2/reports/time',
      GetUsersData: '/v2/Users',
      GetJobsData: '/v2/Jobs',
      ExecuteBatchRequest: '/v2/$batch',
      GetUserInfo: '/v2/Me',
      GetUserCompanyInfo: '/v2/Company',
      CreateUser: '/v2/Users',
      CreateAllocation: '/v2/Allocations',
      CreateJob: '/v2/Jobs',
      CreateTask: '/v2/Tasks',
      CreateTimeEntries: '/v2/TimeEntries',
      CreateDivisions: '/v2/Divisions',
      CreateClients: '/v2/Clients'
    },
    /** List of Request Response Limits */
    RequestResponseLimit: {
      TimeReportLimit: 2500,
      GetUsersLimit: 500,
      GetJobsLimit: 500
    },
    /** Boundary Information for Multipart Requests  */
    MultipartBoundary: {
      Boundary: 'dj8bjmwejx9h4j2zwbgawzmsqy930poik9pm',
      StartingBoundary: '--dj8bjmwejx9h4j2zwbgawzmsqy930poik9pm',
      EndingBoundary: '--dj8bjmwejx9h4j2zwbgawzmsqy930poik9pm--'
    },
    HttpRequestMethods: {
      Get: 'GET',
      Post: 'POST'
    },
    BatchRequestLimit: 49
  },
  /** Entity values exist in Entity table */
  EntityType: {
    Users: 1,
    Jobs: 2,
    Allocations: 3,
    Tasks: 4,
    TimeEntries: 5,
    Divisions: 6,
    Clients: 7
  },
  /** Tables name in SQL database for different entities */
  TableNames: {
    Users: 'Users',
    Allocations: 'Allocations',
    Jobs: 'Jobs',
    Tasks: 'Tasks',
    TimeEntries: 'TimeEntries',
    Divisions: 'Divisions',
    Clients: 'Clients'
  },
  /** Job Status values exist in ImportJobStatus table  */
  ImportJobStatus: {
    NotStarted: 1,
    PendingProcess: 2,
    InProcess: 3,
    Completed: 4,
    CompletedWithError: 5,
    Failed: 6
  },
  AzureFunctions: {
    GetImportJobDetail: 'ImportJobDetail/'
  },
  MaximumNoOfAzureStorageQueues: 20
};
