module.exports = async function (context, myTimer) {
  var timeStamp = new Date().toISOString();
  if (myTimer.IsPastDue) {
    context.log('JavaScript is running late!');
  }
  context.log('Pending  timer trigger function ran!', timeStamp);
  // Call manageAzureStorageQueue method from azure_storage_queue_manager Module to Process pending jobs
  require('../azure_storage_queue_manager').manageAzureStorageQueue({
    queueName: ''
  }).catch(error => {
    console.log('Exception occured while trying to call \'manageAzureStorageQueue\' function from PendingJobsWatcher.');
    console.error(error);
  });
};
