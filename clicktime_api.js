// Node Js Request Module to make Http Requests to CLickTime API
const request = require('request');
// Import ClickTime API constats from App Constants
const ClickTimeAPI = require('./app_constants').ClickTimeAPI;
// Import Database Operations Module
const databaseGeneralOperations = require('./DatabaseOperations/general_operations');
/**
 * This Module provide the methods to consume ClickTime API
 */
module.exports = {
  /**
  * Get User Information By consuming /v2/me endpoint using Authentication Token
  * @param string User Authentication Token
  */
  getUserInfoByAuthToken: async function (authenticationToken) {
    return new Promise((resolve, reject) => {
      try{
        const urlToGetUserDetail = ClickTimeAPI.Protocol.concat(ClickTimeAPI.Host, ClickTimeAPI.Endpoints.GetUserInfo);
        const requestOptions = getRequestrequestOptions({
          httpMethod: ClickTimeAPI.HttpRequestMethods.Get,
          requestUrl: urlToGetUserDetail,
          authToken: authenticationToken
        });
        request(requestOptions, function (error, response, body) {
          // Incase of 429 error code, wait 10 seconds and recall the function.
          if(error != null && error.code === 429) {
            setTimeout(resolve(module.exports.getUserInfoByAuthToken(authenticationToken), 10000));
          } else if(error) {
            reject(error);
          } else{
            var jsonResponse = JSON.parse(body);
            resolve(jsonResponse.data);
          }
        });
      } catch(error) {
        reject(error);
      }
    });
  },
  /**
  * Get Company Information associated with a user By consuming /v2/company endpoint using Authentication Token
  * @param string User Authentication Token
  */
  getUserCompanyInfo: async function (authenticationToken) {
    return new Promise((resolve, reject) => {
      try{
        const urlToGetCompanyInfo = ClickTimeAPI.Protocol.concat(ClickTimeAPI.Host, ClickTimeAPI.Endpoints.GetUserCompanyInfo);
        const requestOptions = getRequestrequestOptions({
          httpMethod: ClickTimeAPI.HttpRequestMethods.Get,
          requestUrl: urlToGetCompanyInfo,
          authToken: authenticationToken
        });
        request(requestOptions, function (error, response, body) {
          // Incase of 429 error code, wait 10 seconds and recall the function.
          if(error != null && error.code === 429) {
            setTimeout(resolve(module.exports.getUserCompanyInfo(authenticationToken), 10000));
          } else if(error) {
            reject(error);
          } else{
            var jsonResponse = JSON.parse(body);
            resolve(jsonResponse.data);
          }
        });
      } catch(error) {
        reject(error);
      }
    });
  },
  /**
  * Get BatchRequest Bodies for Executing Get Requests using Batch Endpoint of ClickTime API
  * @param Aray[] importJobData - No of requests need to generate
  * @param string endPointURL - Endpoint with query string for which batch request generation is requie
  * @return List of Batch Request Bodies generated using supplied parameters
  */
  getBatchRequests: async function ({ importJobData, endpointURL }) {
    return new Promise((resolve, reject) => {
      try{
        const batchRequests = [];
        let multiPartRequestBody = '';
        let batchRequestsRecords = [];
        let batchRequestCounter = 0;
        importJobData.forEach(function (dataObject, index) {
          const userObject = JSON.stringify(dataObject);
          multiPartRequestBody += `${ClickTimeAPI.MultipartBoundary.StartingBoundary}\r\nContent-Type: application/http; msgtype=request\r\n\r\nPOST ${endpointURL} HTTP/1.1\r\nHost: ${ClickTimeAPI.Host}\r\nContent-Type: application/json; charset=utf-8\r\n\r\n${userObject}\r\n\r\n\r\n`;
          batchRequestsRecords.push({ RecordID: dataObject.RecordID, TotalAttempts: dataObject.TotalAttempts });
          batchRequestCounter++;
          const isBatchRequestBodyCompleted = batchRequestCounter === ClickTimeAPI.BatchRequestLimit || index + 1 === importJobData.length;
          if(isBatchRequestBodyCompleted) {
            multiPartRequestBody += ClickTimeAPI.MultipartBoundary.EndingBoundary;
            batchRequestCounter = 0;
            batchRequests.push({ RequestedObjects: batchRequestsRecords, BatchRequestBody: multiPartRequestBody });
            multiPartRequestBody = '';
            batchRequestsRecords = [];
          }
        });
        resolve(batchRequests);
      } catch(ex) {
        reject(ex);
      }
    });
  },
  /**
  * Execute Batch Request
  * @param {string} batchRequest - Batch Request body
  * @param {string} authToken - ClickTime API Authentication Token obtained using /v2/me endpoint
  * @param {Number} reAttemptLimit -  Maximum number of attempt for records failed with API throtal error
  * @return Return the response object on request completion
  */
  executeBatchRequest: async function ({ batchRequest, authToken, reAttemptLimit, entityTypeID }) {
    return new Promise((resolve, reject) => {
      const requestExecutionStatus = { APIThrotalErrorOccuredForRequest: false, RecordsCountForReProcessing: 0 };
      const batchRequestUrl = ClickTimeAPI.Protocol.concat(ClickTimeAPI.Host, ClickTimeAPI.Endpoints.ExecuteBatchRequest);
      const requestOptions = getRequestrequestOptions({
        httpMethod: ClickTimeAPI.HttpRequestMethods.Post,
        requestUrl: batchRequestUrl,
        authToken: authToken,
        requestBody: batchRequest.BatchRequestBody,
        contentType: `multipart/mixed; boundary=${ClickTimeAPI.MultipartBoundary.Boundary}`
      });
      request(requestOptions, async function (error, response, body) {
        if (error) resolve(true);
        try{
          const batchSepratorResponse = getBatchSeparator(response);
          if(batchSepratorResponse.IsBoundaryExist) {
            var responseBodies = body.split(`--${batchSepratorResponse.Boundary}\r\n`);
            if(responseBodies.length > 0) {
              for (let index = 1; index < responseBodies.length; index++) {
                let successfullyCreated = true;
                const recordID = batchRequest.RequestedObjects[index - 1].RecordID;
                let apiDetailResponse = responseBodies[index].split('HTTP/1.1')[1].split('\r\nContent-Type')[0].trim();
                const apiResponseCode = apiDetailResponse.split(' ')[0].trim();
                let createRecordIDFromAPI = null;
                const jsonDataForARequest = extractJSON(responseBodies[index]);
                if(apiResponseCode === '201') {
                  successfullyCreated = true;
                  if(jsonDataForARequest[0].data.hasOwnProperty('ID')) {
                    createRecordIDFromAPI = jsonDataForARequest[0].data.ID;
                  }
                } else if(apiResponseCode === '429') {
                  successfullyCreated = false;
                  requestExecutionStatus.APIThrotalErrorOccuredForJob = true;
                  if(batchRequest.RequestedObjects[index - 1].TotalAttempts < reAttemptLimit) {
                    requestExecutionStatus.RecordsCountForReProcessing++;
                  }
                } else{
                  successfullyCreated = false;
                }
                if(jsonDataForARequest && jsonDataForARequest.length > 0 && jsonDataForARequest[0].errors) {
                  apiDetailResponse += ` --  ${JSON.stringify(jsonDataForARequest[0].errors)}`;
                }
                await databaseGeneralOperations.updateRecordStatus({
                  createRecordIDFromAPI: createRecordIDFromAPI,
                  recordID: recordID,
                  apiResponseCode: apiResponseCode,
                  apiDetailResponse: apiDetailResponse,
                  isSuccessfullyCreated: successfullyCreated,
                  entityTypeID: entityTypeID
                });
              }
            }
          } else{
            requestExecutionStatus.APIThrotalErrorOccuredForJob = false;
          }
        } catch(ex) {
          reject(ex.message);
        }
        resolve(requestExecutionStatus);
      });
    });
  }
};
/**
 * Extract valid json string from plain text return it as JSON object
 * @param string plain text from which json extraction is require
 * @return json object
 */
function extractJSON (str) {
  try{
    var firstOpen, firstClose, candidate;
    firstOpen = str.indexOf('{', firstOpen + 1);
    do {
      firstClose = str.lastIndexOf('}');
      console.log(`firstOpen: ${firstOpen}, firstClose: ${firstClose}`);
      if(firstClose <= firstOpen) {
        return null;
      }
      do {
        candidate = str.substring(firstOpen, firstClose + 1);
        console.log(`candidate: ${candidate}`);
        try {
          var res = JSON.parse(candidate);
          console.log('...found');
          return [res, firstOpen, firstClose + 1];
        } catch(e) {
          console.log('...failed');
        }
        firstClose = str.substr(0, firstClose).lastIndexOf('}');
      } while(firstClose > firstOpen);
      firstOpen = str.indexOf('{', firstOpen + 1);
    } while(firstOpen !== -1);
  } catch(e) {
    console.log(`An exception has been occured while trying to parse response: ${e.message}`);
  }
};

/**
 *@param HttpResponse response - Batch Request Response Object
 */
function getBatchSeparator (batchRequestResponse) {
  const response = { IsBoundaryExist: false, Boundary: '' };
  try{
    if(batchRequestResponse.headers['content-type'].indexOf('boundary') !== -1) {
      response.IsBoundaryExist = true;
      response.Boundary = batchRequestResponse.headers['content-type'].split(';')[1].replace('boundary=', '').replace(/["]/g, '').trim();
    }
  } catch(ex) {
    console.log(ex.message);
  }
  return response;
};

function getRequestrequestOptions ({ httpMethod, requestUrl, authToken, requestBody, contentType }) {
  const requestOptions = {
    method: httpMethod,
    url: requestUrl,
    headers: {
      Expect: '100-continue',
      'cache-control': 'no-cache',
      Connection: 'keep-alive',
      Host: ClickTimeAPI.Host,
      Accept: '*/*',
      Authorization: `Token ${authToken}`
    }
  };
  if(contentType) {
    requestOptions.headers['Content-Type'] = contentType;
  }
  if(requestBody) {
    requestOptions.body = requestBody;
  }
  return requestOptions;
};
