// Import getResponseObject method from general utilities module
const getResponseObject = require('../general_utilities').getResponseObject;
module.exports = async function (context, req) {
  context.log('JavaScript HTTP trigger function processed a request.');
  await require('../general_utilities').validateUsersAuthenticity(req).then(async requestValidation => {
    if (requestValidation.ValidationStatus) {
      require('../azure_storage_queue_manager').manageAzureStorageQueue({
        queueName: ''
      }).then(() => {
        context.res = getResponseObject({
          body: 'Successfully started the execution of pending jobs'
        });
      }).catch(error => {
        console.log('Exception occured while trying to call \'manageAzureStorageQueue\' function from PendingJobsWatcher.');
        console.error(error);
        context.res = getResponseObject({
          status: 500,
          body: error
        });
      });
    } else {
      context.res = getResponseObject({
        status: requestValidation.Status,
        body: requestValidation.ResponseMessage
      });
    }
  }).catch(requestValidation => {
    context.res = getResponseObject({
      status: requestValidation.Status,
      body: requestValidation.ResponseMessage
    });
  });
};
