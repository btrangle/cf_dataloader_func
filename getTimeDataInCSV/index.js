module.exports = async function (context, req) {
    try{
        context.log('JavaScript HTTP trigger function processed a request.');

        // Get Authcode from Azure Function application setting(environment variables)  
        let azureAuthCode = req.query.auth_code;
        // Validate query parameters
        const requestValidation = validateRequest(req,azureAuthCode); 
        if (requestValidation.ValidationStatus) {
            context.log('Parameters OK. Next: get the Token');
            let errorMessage = '';
            // Extract Month Start and End Date from Supllied Parameter
            const reportDate = new Date(req.query.month);
            const year = reportDate.getFullYear();
            const month = reportDate.getMonth();
            const startDate = formatDate(new Date(year, month, 1));
            const endDate = formatDate(new Date(year, month + 1, 0));

            
            // Get API Username and Password from Azure Function application setting(environment variables)  
            const apiUsername =  "TestUsername";
            const apiPassword =  "TestPassword";        
           //const apiUsername =  process.env["APIUsername"];
           //const apiPassword =  process.env["APIPassword"];
            await getAuthToken(apiUsername,apiPassword,azureAuthCode).then(async authenticationToken=> {
                azureAuthCode = authenticationToken;
                context.log('Token Recieved. Next: get the Time Report');
                // Get Time Report for specified month using Batch Request
                await getTimeReportUsingBatchRequest(startDate,endDate,authenticationToken).then(async timeReportData=> {
                    context.log('Time Report Recieved. Next: get the Users List');
                    // Extract unique user IDs from Time Report Data
                    var uniqueUserIDs = [...new Set(timeReportData.map(item => item.UserID))];
                    // Extract unique Job IDs from Time Report Data
                    var uniqueJobIds = [...new Set(timeReportData.map(item => item.JobID))];
                    // Get Data of all Users which exist in Time Report using Batch Request
                    await getUsersDataUsingBatchRequest(uniqueUserIDs,authenticationToken).then(async usersList => {
                        context.log('Users Data Recieved. Next: get the Jobs List');
                        if(usersList && usersList.length > 0){
                            await getJobsDataUsingBatchRequest(uniqueJobIds,authenticationToken).then(jobsListData => {
                                context.log('Job Data Recieved. Next: Process data and generate CSV');
                                if(jobsListData && jobsListData.length > 0){
                                    var responseArray = [];
                                    // Use First date of the month as Effevtive date in mm/dd/yyyy
                                    var effectiveDate = (reportDate.getMonth() + 1)+'/01/'+reportDate.getFullYear();
    
                                    // itterate throug Unique User IDs array to generate report for all jobs of each user
                                    [...uniqueUserIDs].forEach(function(userId) {
                                        // get user detail against currently itterating UserID from Users List
                                        var userDetail = usersList.filter(u => u.ID === userId);
                                        if(userDetail && userDetail.length > 0){
                                            // get all jobs against itterating UserID from Jobs List
                                            var userJobs = timeReportData.filter(t => t.UserID === userId);
                                            // extract unique job IDs from user jobs
                                            var uniqueJobIdsForUser = [...new Set(userJobs.map(item => item.JobID))];
                                            var userTimeReport = [];
                                            // itterate through list of unique job IDs for currently itetrating User ID
                                            [...uniqueJobIdsForUser].forEach(function(jobID) {
                                                // get job detail of currently itterating JOb ID from unique job IDs of user
                                                var jobDetail = jobsListData.filter(j => j.ID === jobID);
                                                // get Time Report for currently itterating JOb ID from unique job IDs of user
                                                var jobTimeDetail = timeReportData.filter(t => t.JobID === jobID);
                                                // extract Job Time detail of currently itterating UserID from Job Time Report
                                                var jobTimeDetailForUser = jobTimeDetail.filter(t => t.UserID === userId);
                                                // Sum of Time spent by user on currently itterating job
                                                var totalTimeForJob = parseFloat(jobTimeDetailForUser.sum("Hours").toFixed(3));
                                                // Sum of Time spent by user on All job
                                                var totalTimeForAllJobs = timeReportData.filter(t=> t.UserID === userId).sum("Hours");
                                                // Calculate Line percentage for currently itterating job by dividing totalTimeForJob/totalTimeForAllJobs
                                                var spentTimePercentage = (totalTimeForJob/totalTimeForAllJobs) * 100;
                                                // Round the percentage to one Decimal Places
                                                spentTimePercentage = parseFloat(spentTimePercentage.toFixed(1));
                                                // Push manuplated data to User Time Report Array
                                                userTimeReport.push({
                                                    'EmployeeNumber': userDetail != null && userDetail[0].EmployeeNumber != null ? userDetail[0].EmployeeNumber.toString() : '',
                                                    'EffectiveDate':  effectiveDate,
                                                    'SpeedkeyCode':   jobDetail != null && jobDetail[0].AccountingPackageID != null ? jobDetail[0].AccountingPackageID : '',
                                                    'LinePercentage': spentTimePercentage,
                                                    'TotalHours': totalTimeForJob
                                                });
                                            });
                                            // Sum of Line Percentage for all jobs for user
                                            let sumOfLinePercentage = userTimeReport.sum("LinePercentage");
                                            // Manuplate the highest value of Line Percentage if sum of all Line Percentage is not equal to 1 
                                            if( !isNaN(sumOfLinePercentage) && sumOfLinePercentage != 100){
                                                let maxValue = Math.max(...userTimeReport.map(r => r.LinePercentage));
                                                let indexOfMaxValue = userTimeReport.indexOf(userTimeReport.filter(u => u.LinePercentage === maxValue)[0]);
                                                let valueEqualizer = (100) - (sumOfLinePercentage);
                                                if(indexOfMaxValue != -1){
                                                    userTimeReport[indexOfMaxValue].LinePercentage = ((userTimeReport[indexOfMaxValue].LinePercentage) + (valueEqualizer)).toFixed(3);
                                                }
                                            }
                                            
                                            // Push User Time Report to Main Response Array
                                            responseArray.push(userTimeReport);
                                        }
                                    });
        
                                    // CSV Lines Array
                                    var lineArray = [];
        
                                    // Show Headers if show_header == 1
                                    if(req.query.show_header == 1){
                                        lineArray.push('Employee Number,Effective Date,Speedkey Code,Line %,Hours');
                                    }
        
                                    // Converting response array to CSV Content
                                    [...responseArray].forEach(function (reportData,index) {
                                    var userData = Object.values(Object.values(reportData));
                                        [...userData].forEach(function (userJobData,index) {
                                            var lineData = Object.values(Object.values(userJobData));
                                            var line = lineData.join(",");
                                            lineArray.push(line);
                                        });
                                    });
        
                                    var csvContent = lineArray.join("\n");
                                    //  Return the payload back to the user in CSV format
                                    context.res = {
                                        status: 200,
                                        body: csvContent,
                                        headers: {
                                            'Content-disposition': 'attachment; filename='+getReportFileName(reportDate),
                                            'Content-Type': 'text/csv'
                                        }
                                    };
                                    context.done();
                                }else{
                                    context.res = {
                                        status: 500,
                                        body: 'Unable to get Jobs Data'
                                    };
                                    context.done();
                                }
                            }).catch(error=> {
                                errorMessage = error.message;
                            });
                        }else{
                            context.res = {
                                status: 500,
                                body: 'Unable to get user Data'
                            };
                            context.done();
                        }
                    }).catch(error=> {
                        errorMessage = error.message;
                    });
                }).catch(error=> {
                    errorMessage = error.message;
                });
            }).catch(error => {
                errorMessage = error.message;
            });

            if(errorMessage.length > 0){
                //  Return the payload back to the user in CSV format
                context.res = {
                    status: 500,
                    body: errorMessage
                };
                context.done();
            } 
        }
        else {
            // Response back with Status 400 if request parameters are not valid 
            context.res = {
                status: 400,
                body: requestValidation.ResponseMessage
            };
            context.done();
        }
    }catch(ex){
        console.log(ex.message);
    }
};

// Boundary Information for Multipart Requests
const multipartBoundary = {
    Boundary:'dj8bjmwejx9h4j2zwbgawzmsqy930poik9pm',
    StartingBoundary: '--dj8bjmwejx9h4j2zwbgawzmsqy930poik9pm',
    EndingBoundary: '--dj8bjmwejx9h4j2zwbgawzmsqy930poik9pm--'
}

/**
* ClickTime API Properties
*/
const clickTimeAPI = {
    Protocol: 'https://',
    Host: 'api.clicktime.com',
    /** List of ClickTime API Endpoints */
    Endpoints:{
        GetTimeReport:'/v2/reports/time',
        GetUsersData: '/v2/users',
        GetJobsData: '/v2/jobs',
        ExecuteBatchRequest: '/v2/$batch',
        GetAuthToken: '/v2/me'
    },
    /** List of Request Response Limits */
    RequestResponseLimit:{
        TimeReportLimit:2500,
        GetUsersLimit:100,
        GetJobsLimit:500
    }
}

// Node Js Request Module to make Http Requests to CLickTime API
var request = require("request");
// ES6-Promise-TRY module to manage exceptions in Promise
var promiseTry = require("es6-promise-try");

/**
 * Get authentication token for consuming ClickTime API By consuming /v2/me endpoint using Basic authentication 
 * @param string API Username from Azure Function application setting(environment variables)  
 * @param string API Password from Azure Function application setting(environment variables)   
 */
async function getAuthToken(apiUsername,apiPassword,auth){
    return new Promise((resolve, reject) => {
        try{
            let apiCredentials = apiUsername+':'+apiPassword;
            let buff = new Buffer(apiCredentials);
            let base64EncodedCredentials = buff.toString('base64');
            const urlToGetAutToken = clickTimeAPI.Protocol.concat(clickTimeAPI.Host,clickTimeAPI.Endpoints.GetAuthToken);
            var options = { method: 'GET',
                url: urlToGetAutToken,
                headers: 
                { 'cache-control': 'no-cache',
                    Connection: 'keep-alive',
                    'Accept-Encoding': 'gzip, deflate',
                    Host: clickTimeAPI.Host,
                    'Cache-Control': 'no-cache',
                    Accept: '*/*',
                    Authorization: 'Token '+auth
                    //Authorization: 'Basic aW1yYW5AcmVwb3J0ZXEuY29tOjcyNzNzdGVw' 
                } 
            };

            request(options, function (error, response, body) {
                // Incase of 429 error code, wait 10 seconds and recall the function.
                if(error != null && error.code == 429){
                    setTimeout(resolve(getAuthToken(),10000));
                }
                else if(error){
                    reject(error);
                }
                //console.log(body);
                var jsonResponse = JSON.parse(body);
                resolve(jsonResponse.data.AuthToken.Token);
            });
        }
        catch(error){
            reject(error);
        }
    });
}


/**
 * Validate Http Request Parameters 
 * @param HttpRequestObject HttpRequest Object 
 * @param string Azure Application Setting 
 * @return boolean true or false
 * IF request is valid return true
 */
var validateRequest = function(req,azureAuthCode){
    let responseMessage = 'Valid Request';
    let validationStatus = true;
    if (!req.query.auth_code || !req.query.month || !req.query.show_header) {
        validationStatus = false;
        responseMessage = 'Please specify values for all parameters in query string';
    }
    else if(req.query.auth_code != azureAuthCode){
        validationStatus = false;
        responseMessage = 'Invalid Authentication Code';
    }
    else if(!isValidDate(req.query.month)){
        validationStatus = false;
        responseMessage = 'Invalid Date parameter';
    }
    return {ValidationStatus:validationStatus,ResponseMessage:responseMessage};
}

/**
 * Calculate Number of requests required to get all data for specific endpoint and parameters
 * @param string endpointPath dd
 * @param string queryString 
 * @param Integer responseLimit 
 * @param string authToken 
 */
async function calculateNumberOfRequests(endpointPath,queryString,responseLimit,authToken){
    return new Promise((resolve, reject) => {
        const endpointUrl = clickTimeAPI.Protocol.concat(clickTimeAPI.Host,endpointPath);
        var options = { method: 'GET',
            url: endpointUrl,
            qs: queryString,
            headers: 
            { 
                'cache-control': 'no-cache',
                Connection: 'keep-alive',
                'Accept-Encoding': 'gzip, deflate',
                Host: clickTimeAPI.Host,
                'Cache-Control': 'no-cache',
                Accept: '*/*',
                'Content-Type': 'application/json',
                Authorization: 'Token '+authToken
            } 
        };

        request(options, function (error, response, body) {
            if (error) reject(error);
            var jsonResponse = tryParseJSON(body);
            if(jsonResponse !== false){
                var totalRecordCount = jsonResponse.page.count;
                var totalRequests = totalRecordCount/responseLimit;
                if (isDecimalNumber(totalRequests)){
                    let integerPart = totalRequests.toString().split('.')[0];
                    totalRequests = parseInt(integerPart)+1;
                }
                resolve(totalRequests);
            }else{
                resolve(0);
            }
            
        });

    });
}


/**
 * Get BatchRequest Body for Executing Get Requests using Batch Endpoint of ClickTime API
 * @param integer totalRequests - No of requests need to generate
 * @param string endpointWithQueryString - Endpoint with query string for which batch request generation is requie
 * @param integer responseLimit - Maximum Response Limit to handle Offset
 * @return Batch Request Body generated using supplied parameters 
 */
async function getBatchRequestBody(totalRequests,endpointWithQueryString,responseLimit){
    return new Promise((resolve, reject) => {
        let multiPartRequestBody = '';
        for (let index = 0; index < totalRequests; index++) {
            let offset = responseLimit * index;
            let endpointUrlWithAllParameters = `${endpointWithQueryString}${endpointWithQueryString.indexOf('?') === -1 ? '?limit'  : '&limit' }=${responseLimit}&offset=${offset}`;
            multiPartRequestBody += `${multipartBoundary.StartingBoundary}\r\nContent-Type: application/http; msgtype=request\r\n\r\nGET ${endpointUrlWithAllParameters} HTTP/1.1\r\nHost: ${clickTimeAPI.Host}\r\n\r\n\r\n`;

            if(index+1 == totalRequests){
                multiPartRequestBody += multipartBoundary.EndingBoundary;
            }
        }
        resolve(multiPartRequestBody);
    });
}

/**
 * Get Time Report for a date range supplied as parameter, using Batch Endpoint of CLickTime API
 * @param startDate Start Date 
 * @param endDate End Date 
 * @param string ClickTime API Authentication Token obtained using /v2/me endpoint
 * @return Array of Time Report objects for date range supplied as parameter
 */
async function getTimeReportUsingBatchRequest(startData,endDate,authToken){
    return new Promise(async (resolve, reject) => {
        try{
            // Query string to get total record count for calculation of Number of requests to get all data
            const queryString = { StartDate: startData, EndDate: endDate, limit:0 };

            // Get total number of requests required to get all data
            let totalRequests = await calculateNumberOfRequests(clickTimeAPI.Endpoints.GetTimeReport,
                queryString,
                clickTimeAPI.RequestResponseLimit.TimeReportLimit,
                authToken
            );

            // Conactinate query string parameters with Time Report endpoint
            const endpointWithQueryString = clickTimeAPI.Endpoints.GetTimeReport+'?StartDate='+startData+'&EndDate='+endDate;

            // Get Multipart Request body
            let multiPartRequestBody = await getBatchRequestBody(totalRequests,
                endpointWithQueryString,
                clickTimeAPI.RequestResponseLimit.TimeReportLimit
            );

            // Execute Batch Request using multiPartRequestBody
            var timeReportData = await executeBatchRequest(multiPartRequestBody,authToken);
            // extract array from type object
            timeReportData = Object.values(timeReportData);
            resolve(timeReportData);
        }
        catch(error){
            // Incase of 429 error code, wait 10 seconds and recall the function.
            if(error != null && error.code == 429){
                setTimeout(resolve(getTimeReportUsingBatchRequest(startData,endDate,authToken),10000));
            }
            else{
                reject(error);
            }
        }
        
    });
}

/**
 * Get Users Data for specified UserIDs using Batch Endpoint of CLickTime API
 * @param StringArray Array of userIDS for which data is required
 * @param string ClickTime API Authentication Token obtained using /v2/me endpoint
 * @return Array of User's Data for supplied UserIDs array
 */
async function getUsersDataUsingBatchRequest(userIDS,authToken){
    return new Promise(async(resolve, reject) => {
        try{
            // Query string to get total record count for calculation of Number of requests to get all data
            // Get total number of requests required to get all data
            const totalRequests = await calculateNumberOfRequests(clickTimeAPI.Endpoints.GetUsersData,
                '',
                clickTimeAPI.RequestResponseLimit.GetUsersLimit,
                authToken
            );

            let endpointToGetUsersList = clickTimeAPI.Endpoints.GetUsersData;
            // Get Multipart Request body
            let multiPartRequestBody = await getBatchRequestBody(totalRequests,
                endpointToGetUsersList,
                clickTimeAPI.RequestResponseLimit.GetUsersLimit
            );
            // Execute Batch Request using multiPartRequestBody
            var usersData = await executeBatchRequest(multiPartRequestBody,authToken);
            // extract array from type object
            usersData = Object.values(usersData);
            resolve(usersData);
        }
        catch(error){
             // Incase of 429 error code, wait 10 seconds and recall the function.
             if(error != null && error.code == 429){
                setTimeout(resolve(getTimeReportUsingBatchRequest(startData,endDate,authToken),10000));
            }
            else{
                reject(error);
            }
        }
    });
}

/**
 * Get Jobs Data for specified JobIDs using Batch Endpoint of CLickTime API
 * @param StringArray Array of jobIDs for which data is required
 * @param string  ClickTime API Authentication Token obtained using /v2/me endpoint 
 * @return Array of Jobs's Data for supplied jobIDs array
 */
async function getJobsDataUsingBatchRequest(jobIDs,authToken){
    return new Promise(async(resolve, reject) => {
        try{
            // Query string to get total record count for calculation of Number of requests to get all data
            const queryString = { 'limit':0, 'id[]':jobIDs };
            // Get total number of requests required to get all data
            const totalRequests = await calculateNumberOfRequests(clickTimeAPI.Endpoints.GetJobsData,
                queryString,
                clickTimeAPI.RequestResponseLimit.GetJobsLimit, 
                authToken
            );
            
            if(totalRequests != 0){
                // Conactinate query string parameters with User endpoint
                let endpointWithQueryString = clickTimeAPI.Endpoints.GetJobsData+'?id[]='+jobIDs[0];
                for (let index = 1; index < jobIDs.length; index++) {
                    endpointWithQueryString += '&id[]='+jobIDs[index];
                }

                // Get Multipart Request body
                let multiPartRequestBody = await getBatchRequestBody(totalRequests,
                    endpointWithQueryString,
                    clickTimeAPI.RequestResponseLimit.TimeReportLimit
                );
                // Execute Batch Request using multiPartRequestBody
                var jobsData = await executeBatchRequest(multiPartRequestBody,authToken);
                // extract array from type object
                jobsData = Object.values(jobsData);
                resolve(jobsData);
            }else{
                resolve([]);
            }
        }
        catch(error){
            // Incase of 429 error code, wait 10 seconds and recall the function.
            if(error != null && error.code == 429){
                setTimeout(resolve(getTimeReportUsingBatchRequest(startData,endDate,authToken),10000));
            }
            else{
                reject(error);
            }
        }
    });
}


/**
 * Execute Batch Request
 * @param string Batch Request body
 * @param string ClickTime API Authentication Token obtained using /v2/me endpoint
 * @return Response of all requests in request body as a single combined array object
 */
async function executeBatchRequest(requestBody,authToken){
    return new Promise((resolve, reject) => {
        const batchRequestUrl = clickTimeAPI.Protocol.concat(clickTimeAPI.Host,clickTimeAPI.Endpoints.ExecuteBatchRequest);
        var options = { method: 'POST',
            url: batchRequestUrl,
            headers: 
            { 
                Expect: '100-continue',
                'cache-control': 'no-cache',
                Connection: 'keep-alive',
                Host: clickTimeAPI.Host,
                Accept: '*/*',
                Authorization: 'Token '+authToken,
                'Content-Type': 'multipart/mixed; boundary="'+multipartBoundary.Boundary+'"',
                
            },
            body: requestBody
        };

        request(options, function (error, response, body) {
            if (error) reject(error);
            var parsedData = [];
            try{
                var responseBodies = body.split('Content-Type: application/json; charset=utf-8');
                if(responseBodies.length > 0 && responseBodies[0].indexOf('HTTP/1.1 200 OK') != -1){
                   for (let index = 1; index < responseBodies.length; index++) {
                       const jsonDataForARequest = extractJSON(responseBodies[index]);
                       if(jsonDataForARequest != null && jsonDataForARequest.length > 0){
                            parsedData = parsedData.concat(jsonDataForARequest[0].data);
                       }
                   }
                }
                
            }
            catch(e){

            }
            resolve(parsedData);
        });

    });
}


/**
 * isValidDate(str)
 * @param string str value yyyy-mm-dd
 * @return boolean true or false
 * IF date is valid return true
 */
var isValidDate = function (str){
    // STRING FORMAT yyyy-mm-dd
    if(str=="" || str==null){return false;}								
    
    // m[1] is year 'YYYY' * m[2] is month 'MM' * m[3] is day 'DD'					
    var m = str.match(/(\d{4})-(\d{2})-(\d{2})/);
    
    // STR IS NOT FIT m IS NOT OBJECT
    if( m === null || typeof m !== 'object'){return false;}				
    
    // CHECK m TYPE
    if (typeof m !== 'object' && m !== null && m.size!==3){return false;}
                
    var ret = true; //RETURN VALUE						
    var thisYear = new Date().getFullYear(); //YEAR NOW
    var minYear = 1999; //MIN YEAR
    
    // YEAR CHECK
    if( (m[1].length < 4) || m[1] < minYear || m[1] > thisYear){ret = false;}
    // MONTH CHECK			
    if( (m[2].length < 2) || m[2] < 1 || m[2] > 12){ret = false;}
    // DAY CHECK
    if( (m[3].length < 2) || m[3] < 1 || m[3] > 31){ret = false;}
    
    return ret;			
}

var tryParseJSON = function (jsonString) {
    try {
      var o = JSON.parse(jsonString);
      // Handle non-exception-throwing cases:
      // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
      // but... JSON.parse(null) returns null, and typeof null === "object",
      // so we must check for that, too. Thankfully, null is falsey, so this suffices:
      if (o) {
        return o;
      }
    } catch (e) { }
    return false;
  }

/**
 * Format the supplied data as yyyy-mm-dd
 * @param Date date
 * @return Formated date as yyyy-mm-dd
 */
var formatDate = function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

/**
 * Extract valid json string from plain text return it as JSON object
 * @param string plain text from which json extraction is require
 * @return json object
 */
var extractJSON =function extractJSON(str) {
    try{
        var firstOpen, firstClose, candidate;
        firstOpen = str.indexOf('{', firstOpen + 1);
        do {
            firstClose = str.lastIndexOf('}');
            console.log('firstOpen: ' + firstOpen, 'firstClose: ' + firstClose);
            if(firstClose <= firstOpen) {
                return null;
            }
            do {
                candidate = str.substring(firstOpen, firstClose + 1);
                console.log('candidate: ' + candidate);
                try {
                    var res = JSON.parse(candidate);
                    console.log('...found');
                    return [res, firstOpen, firstClose + 1];
                }
                catch(e) {
                    console.log('...failed');
                }
                firstClose = str.substr(0, firstClose).lastIndexOf('}');
            } while(firstClose > firstOpen);
            firstOpen = str.indexOf('{', firstOpen + 1);
        } while(firstOpen != -1);
    }
    catch(e){
        console.log('An exception has been occured while trying to parse response: '+e.message);
    }
}


/**
 * Check if a number is Decimal or Not 
 * @param Number numberToCheck 
 * @return boolean true or false
 * IF suuplied parameter is Decimal return true
 */
var isDecimalNumber = function(numberToCheck){
    return (numberToCheck - Math.floor(numberToCheck)) !== 0;
}

/**
 * Get File Name for Generated CSV 
 * @param Date Report Date supplied as parameter
 * @return Report Name like 'TimeReport_MonthName_Year'
 */
var getReportFileName = function(reportDate){
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    const reportFileName = 'TimeReport_'+monthNames[reportDate.getMonth()]+'_'+reportDate.getFullYear()+'.csv';
    return reportFileName;
}

/**
 * Sum all items in a array for a specific key
 * @param string Item Key
 * @return Summ of all items for specified key
 */
Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][prop]
    }
    return total
}