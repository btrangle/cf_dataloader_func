/** Database Scheema Start **/

/****** Object:  Table [dbo].[Allocations]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Allocations](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobID] [int] NOT NULL,
	[EstHours] [int] NULL,
	[JobID] [nvarchar](68) NULL,
	[Month] [nvarchar](10) NULL,
	[UserID] [nvarchar](68) NULL,
	[CreatedSuccessfully] [bit] NULL,
	[APIResponseCode] [nvarchar](5) NULL,
	[APIDetailResponse] [nvarchar](2000) NULL,
	[ImportJobComments] [nvarchar](150) NULL,
	[TotalAttempts] [int] NULL,
 CONSTRAINT [PK_ImportAllocationsJobData] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomFieldsData]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomFieldsData](
	[CustomFieldID] [int] IDENTITY(1,1) NOT NULL,
	[EntityTypeID] [int] NOT NULL,
	[RecordID] [int] NOT NULL,
	[FieldName] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_UserDataCustomField] PRIMARY KEY CLUSTERED 
(
	[CustomFieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DataImportJob]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataImportJob](
	[ImportJobID] [int] IDENTITY(1,1) NOT NULL,
	[EntityTypeID] [int] NOT NULL,
	[UserID] [nvarchar](50) NOT NULL,
	[UserEmail] [nvarchar](255) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[CompanyID] [nvarchar](50) NULL,
	[TotalRecords] [int] NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[ImportJobStatusID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Comments] [nvarchar](2500) NULL,
	[AuthToken] [nvarchar](500) NULL,
 CONSTRAINT [PK_DataImportJobs] PRIMARY KEY CLUSTERED 
(
	[ImportJobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EntityType]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntityType](
	[EntityTypeID] [int] IDENTITY(1,1) NOT NULL,
	[EntityType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ImportType] PRIMARY KEY CLUSTERED 
(
	[EntityTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImportJobStatus]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImportJobStatus](
	[ImportJobStatusID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobStatus] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ImportJobStatus] PRIMARY KEY CLUSTERED 
(
	[ImportJobStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Jobs]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jobs](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobID] [int] NOT NULL,
	[AccountingPackageID] [nvarchar](68) NULL,
	[BillingRate] [money] NULL,
	[ClientID] [nvarchar](68) NULL,
	[IsActive] [bit] NULL,
	[IsBillable] [bit] NULL,
	[JobNumber] [nvarchar](68) NULL,
	[Name] [nvarchar](50) NULL,
	[Notes] [nvarchar](50) NULL,
	[ProjectManagerID] [nvarchar](68) NULL,
	[SecondaryBillingRateMode] [nvarchar](68) NULL,
	[TimeRequiresApproval] [bit] NULL,
	[UseCompanyBillingRate] [bit] NULL,
	[ID] [nvarchar](68) NULL,
	[CreatedSuccessfully] [bit] NULL,
	[APIResponseCode] [nvarchar](5) NULL,
	[APIDetailResponse] [nvarchar](2000) NULL,
	[ImportJobComments] [nvarchar](150) NULL,
	[TotalAttempts] [int] NULL,
 CONSTRAINT [PK_ImportJobsJobData] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobID] [int] NOT NULL,
	[AccountingPackageID] [nvarchar](68) NULL,
	[BillingRate] [money] NULL,
	[IsActive] [bit] NULL,
	[IsBillable] [bit] NULL,
	[Name] [nvarchar](50) NULL,
	[Notes] [nvarchar](2500) NULL,
	[TaskCode] [nvarchar](68) NULL,
	[UseCompanyBillingRate] [bit] NULL,
	[ID] [nvarchar](68) NULL,
	[CreatedSuccessfully] [bit] NULL,
	[APIResponseCode] [nvarchar](5) NULL,
	[APIDetailResponse] [nvarchar](2000) NULL,
	[ImportJobComments] [nvarchar](150) NULL,
	[TotalAttempts] [int] NULL,
 CONSTRAINT [PK_ImportTasksJobData] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeEntries]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeEntries](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobID] [int] NOT NULL,
	[BillingRate] [money] NULL,
	[BreakTime] [nvarchar](50) NULL,
	[Comment] [nvarchar](2500) NULL,
	[CostRate] [money] NULL,
	[Date] [nvarchar](50) NULL,
	[EndTime] [nvarchar](50) NULL,
	[Hours] [nvarchar](50) NULL,
	[JobID] [nvarchar](68) NULL,
	[StartTime] [nvarchar](50) NULL,
	[TaskID] [nvarchar](68) NULL,
	[UserID] [nvarchar](68) NULL,
	[ID] [nvarchar](68) NULL,
	[CreatedSuccessfully] [bit] NULL,
	[APIResponseCode] [nvarchar](5) NULL,
	[APIDetailResponse] [nvarchar](2000) NULL,
	[ImportJobComments] [nvarchar](150) NULL,
	[TotalAttempts] [int] NULL,
 CONSTRAINT [PK_ImportTimeEntriesJobData] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2/6/2020 2:47:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobID] [int] NOT NULL,
	[AccountingPackageID] [nvarchar](68) NULL,
	[AllowIncompleteTimesheetSubmission] [bit] NULL,
	[BillingRate] [money] NULL,
	[CostRate] [money] NULL,
	[DefaultExpenseTypeID] [nvarchar](68) NULL,
	[DefaultPaymentTypeID] [nvarchar](68) NULL,
	[DefaultTaskID] [nvarchar](68) NULL,
	[DivisionID] [nvarchar](68) NULL,
	[Email] [nvarchar](255) NULL,
	[EmployeeNumber] [nvarchar](68) NULL,
	[EmploymentTypeID] [nvarchar](68) NULL,
	[EnableBreakTime] [bit] NULL,
	[EndDate] [nvarchar](50) NULL,
	[ExpenseApproverID] [nvarchar](68) NULL,
	[GDPRConsentStatus] [nvarchar](50) NULL,
	[ID] [nvarchar](68) NULL,
	[IsActive] [bit] NULL,
	[MinimumTimeHours] [float] NULL,
	[MinimumTimePeriod] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Notes] [nvarchar](255) NULL,
	[PayrollType] [nvarchar](68) NULL,
	[PreferredTimeEntryView] [nvarchar](50) NULL,
	[PreferredTimeFormat] [nvarchar](50) NULL,
	[RequireComments] [bit] NULL,
	[RequireStartEndTime] [bit] NULL,
	[RequireStopwatch] [bit] NULL,
	[Role] [nvarchar](50) NULL,
	[SecurityLevel] [nvarchar](50) NULL,
	[SkipWeekend] [bit] NULL,
	[StartDate] [nvarchar](50) NULL,
	[SubjectToExpenseApproval] [bit] NULL,
	[SubjectToTimesheetApproval] [bit] NULL,
	[SubjectToTimesheetCompletion] [bit] NULL,
	[TimeOffApproverID] [nvarchar](50) NULL,
	[TimesheetApproverID] [nvarchar](50) NULL,
	[UseCompanyBillingRate] [bit] NULL,
	[CreatedSuccessfully] [bit] NULL,
	[APIResponseCode] [nvarchar](5) NULL,
	[APIDetailResponse] [nvarchar](2000) NULL,
	[ImportJobComments] [nvarchar](150) NULL,
	[TotalAttempts] [int] NULL,
 CONSTRAINT [PK_ImportUsersJobData] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Allocations] ADD  CONSTRAINT [DF_ImportAllocationsJobData_TotalAttempts]  DEFAULT ((0)) FOR [TotalAttempts]
GO
ALTER TABLE [dbo].[Jobs] ADD  CONSTRAINT [DF_ImportJobsJobData_TotalAttempts]  DEFAULT ((0)) FOR [TotalAttempts]
GO
ALTER TABLE [dbo].[Tasks] ADD  CONSTRAINT [DF_ImportTasksJobData_TotalAttempts]  DEFAULT ((0)) FOR [TotalAttempts]
GO
ALTER TABLE [dbo].[TimeEntries] ADD  CONSTRAINT [DF_ImportTimeEntriesJobData_TotalAttempts]  DEFAULT ((0)) FOR [TotalAttempts]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_ImportUserJobData_TotalAttempts]  DEFAULT ((0)) FOR [TotalAttempts]
GO
ALTER TABLE [dbo].[Allocations]  WITH CHECK ADD  CONSTRAINT [FK_ImportAllocationsJobData_DataImportJob] FOREIGN KEY([ImportJobID])
REFERENCES [dbo].[DataImportJob] ([ImportJobID])
GO
ALTER TABLE [dbo].[Allocations] CHECK CONSTRAINT [FK_ImportAllocationsJobData_DataImportJob]
GO
ALTER TABLE [dbo].[CustomFieldsData]  WITH CHECK ADD  CONSTRAINT [FK_DataCustomField_EntityType] FOREIGN KEY([EntityTypeID])
REFERENCES [dbo].[EntityType] ([EntityTypeID])
GO
ALTER TABLE [dbo].[CustomFieldsData] CHECK CONSTRAINT [FK_DataCustomField_EntityType]
GO
ALTER TABLE [dbo].[DataImportJob]  WITH CHECK ADD  CONSTRAINT [FK_DataImportJob_EntityType] FOREIGN KEY([EntityTypeID])
REFERENCES [dbo].[EntityType] ([EntityTypeID])
GO
ALTER TABLE [dbo].[DataImportJob] CHECK CONSTRAINT [FK_DataImportJob_EntityType]
GO
ALTER TABLE [dbo].[DataImportJob]  WITH CHECK ADD  CONSTRAINT [FK_DataImportJob_ImportJobStatus] FOREIGN KEY([ImportJobStatusID])
REFERENCES [dbo].[ImportJobStatus] ([ImportJobStatusID])
GO
ALTER TABLE [dbo].[DataImportJob] CHECK CONSTRAINT [FK_DataImportJob_ImportJobStatus]
GO
ALTER TABLE [dbo].[Jobs]  WITH CHECK ADD  CONSTRAINT [FK_ImportJobsJobData_DataImportJob] FOREIGN KEY([ImportJobID])
REFERENCES [dbo].[DataImportJob] ([ImportJobID])
GO
ALTER TABLE [dbo].[Jobs] CHECK CONSTRAINT [FK_ImportJobsJobData_DataImportJob]
GO
ALTER TABLE [dbo].[Tasks]  WITH CHECK ADD  CONSTRAINT [FK_ImportTasksJobData_DataImportJob] FOREIGN KEY([ImportJobID])
REFERENCES [dbo].[DataImportJob] ([ImportJobID])
GO
ALTER TABLE [dbo].[Tasks] CHECK CONSTRAINT [FK_ImportTasksJobData_DataImportJob]
GO
ALTER TABLE [dbo].[TimeEntries]  WITH CHECK ADD  CONSTRAINT [FK_ImportTimeEntriesJobData_DataImportJob] FOREIGN KEY([ImportJobID])
REFERENCES [dbo].[DataImportJob] ([ImportJobID])
GO
ALTER TABLE [dbo].[TimeEntries] CHECK CONSTRAINT [FK_ImportTimeEntriesJobData_DataImportJob]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_ImportUserJobData_Importob] FOREIGN KEY([ImportJobID])
REFERENCES [dbo].[DataImportJob] ([ImportJobID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_ImportUserJobData_Importob]
GO


/** Database Scheema End **/

/** Data for Lookup tables Start **/

GO
SET IDENTITY_INSERT [dbo].[EntityType] ON 
GO
INSERT [dbo].[EntityType] ([EntityTypeID], [EntityType]) VALUES (1, N'Users')
GO
INSERT [dbo].[EntityType] ([EntityTypeID], [EntityType]) VALUES (2, N'Jobs')
GO
INSERT [dbo].[EntityType] ([EntityTypeID], [EntityType]) VALUES (3, N'Allocations
')
GO
INSERT [dbo].[EntityType] ([EntityTypeID], [EntityType]) VALUES (4, N'Tasks')
GO
INSERT [dbo].[EntityType] ([EntityTypeID], [EntityType]) VALUES (5, N'Time Entries')
GO
INSERT [dbo].[EntityType] ([EntityTypeID], [EntityType]) VALUES (6, N'Divisions')
GO
INSERT [dbo].[EntityType] ([EntityTypeID], [EntityType]) VALUES (7, N'Clients')
GO
SET IDENTITY_INSERT [dbo].[EntityType] OFF
GO
SET IDENTITY_INSERT [dbo].[ImportJobStatus] ON 
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (1, N'Not Started')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (2, N'Penidng Process')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (3, N'In Process')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (4, N'Completed')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (5, N'Completed With Errors')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (6, N'Failed')
GO
SET IDENTITY_INSERT [dbo].[ImportJobStatus] OFF
GO

/** Data for Lookup tables End **/