/****** Database Schema    Script Date: 1/22/2020 11:13:31 PM ******/

/****** Object:  Table [dbo].[DataImportJob]    Script Date: 1/22/2020 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataImportJob](
	[ImportJobID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[UserID] [nvarchar](50) NOT NULL,
	[UserEmail] [nvarchar](255) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[CompanyID] [nvarchar](50) NULL,
	[TotalRecords] [int] NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[ImportJobStatusID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Comments] [nvarchar](2500) NULL,
	[AuthToken] [nvarchar](500) NULL,
 CONSTRAINT [PK_DataImportJobs] PRIMARY KEY CLUSTERED 
(
	[ImportJobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entity]    Script Date: 1/22/2020 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entity](
	[EntityID] [int] IDENTITY(1,1) NOT NULL,
	[EntityName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ImportType] PRIMARY KEY CLUSTERED 
(
	[EntityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImportAllocationsJobData]    Script Date: 1/22/2020 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImportAllocationsJobData](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobID] [int] NOT NULL,
	[EstHours] [int] NULL,
	[JobID] [nvarchar](68) NULL,
	[Month] [nvarchar](10) NULL,
	[UserID] [nvarchar](68) NULL,
	[CreatedSuccessfully] [bit] NULL,
	[APIResponseCode] [nvarchar](5) NULL,
	[APIDetailResponse] [nvarchar](2000) NULL,
	[ImportJobComments] [nvarchar](150) NULL,
	[TotalAttempts] [int] NULL,
 CONSTRAINT [PK_ImportAllocationsJobData] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImportJobsJobData]    Script Date: 1/22/2020 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImportJobsJobData](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobID] [int] NOT NULL,
	[AccountingPackageID] [nvarchar](68) NULL,
	[BillingRate] [money] NULL,
	[ClientID] [nvarchar](68) NULL,
	[IsActive] [bit] NULL,
	[IsBillable] [bit] NULL,
	[JobNumber] [nvarchar](68) NULL,
	[Name] [nvarchar](50) NULL,
	[Notes] [nvarchar](50) NULL,
	[ProjectManagerID] [nvarchar](68) NULL,
	[SecondaryBillingRateMode] [nchar](50) NULL,
	[TimeRequiresApproval] [bit] NULL,
	[UseCompanyBillingRate] [bit] NULL,
	[CreatedSuccessfully] [bit] NULL,
	[APIResponseCode] [nvarchar](5) NULL,
	[APIDetailResponse] [nvarchar](2000) NULL,
	[ImportJobComments] [nvarchar](150) NULL,
	[TotalAttempts] [int] NULL,
 CONSTRAINT [PK_ImportJobsJobData] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImportJobStatus]    Script Date: 1/22/2020 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImportJobStatus](
	[ImportJobStatusID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobStatus] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ImportJobStatus] PRIMARY KEY CLUSTERED 
(
	[ImportJobStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImportUserJobData]    Script Date: 1/22/2020 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImportUserJobData](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ImportJobID] [int] NOT NULL,
	[AccountingPackageID] [nvarchar](68) NULL,
	[AllowIncompleteTimesheetSubmission] [bit] NULL,
	[BillingRate] [money] NULL,
	[CostRate] [money] NULL,
	[DefaultExpenseTypeID] [nvarchar](68) NULL,
	[DefaultPaymentTypeID] [nvarchar](68) NULL,
	[DefaultTaskID] [nvarchar](68) NULL,
	[DivisionID] [nvarchar](68) NULL,
	[Email] [nvarchar](255) NULL,
	[EmployeeNumber] [nvarchar](68) NULL,
	[EmploymentTypeID] [nvarchar](68) NULL,
	[EnableBreakTime] [bit] NULL,
	[EndDate] [nvarchar](50) NULL,
	[ExpenseApproverID] [nvarchar](68) NULL,
	[GDPRConsentStatus] [nvarchar](50) NULL,
	[ID] [nvarchar](68) NULL,
	[IsActive] [bit] NULL,
	[MinimumTimeHours] [float] NULL,
	[MinimumTimePeriod] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Notes] [nvarchar](255) NULL,
	[PayrollType] [nvarchar](68) NULL,
	[PreferredTimeEntryView] [nvarchar](50) NULL,
	[PreferredTimeFormat] [nvarchar](50) NULL,
	[RequireComments] [bit] NULL,
	[RequireStartEndTime] [bit] NULL,
	[RequireStopwatch] [bit] NULL,
	[Role] [nvarchar](50) NULL,
	[SecurityLevel] [nvarchar](50) NULL,
	[SkipWeekend] [bit] NULL,
	[StartDate] [nvarchar](50) NULL,
	[SubjectToExpenseApproval] [bit] NULL,
	[SubjectToTimesheetApproval] [bit] NULL,
	[SubjectToTimesheetCompletion] [bit] NULL,
	[TimeOffApproverID] [nvarchar](50) NULL,
	[TimesheetApproverID] [nvarchar](50) NULL,
	[UseCompanyBillingRate] [bit] NULL,
	[CreatedSuccessfully] [bit] NULL,
	[APIResponseCode] [nvarchar](5) NULL,
	[APIDetailResponse] [nvarchar](2000) NULL,
	[ImportJobComments] [nvarchar](150) NULL,
	[TotalAttempts] [int] NULL,
 CONSTRAINT [PK_ImportUsersJobData] PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserDataCustomField]    Script Date: 1/22/2020 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDataCustomField](
	[CustomFieldID] [int] IDENTITY(1,1) NOT NULL,
	[RecordID] [int] NOT NULL,
	[FieldName] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_UserDataCustomField] PRIMARY KEY CLUSTERED 
(
	[CustomFieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImportAllocationsJobData] ADD  CONSTRAINT [DF_ImportAllocationsJobData_TotalAttempts]  DEFAULT ((0)) FOR [TotalAttempts]
GO
ALTER TABLE [dbo].[ImportJobsJobData] ADD  CONSTRAINT [DF_ImportJobsJobData_TotalAttempts]  DEFAULT ((0)) FOR [TotalAttempts]
GO
ALTER TABLE [dbo].[ImportUserJobData] ADD  CONSTRAINT [DF_ImportUserJobData_TotalAttempts]  DEFAULT ((0)) FOR [TotalAttempts]
GO
ALTER TABLE [dbo].[DataImportJob]  WITH CHECK ADD  CONSTRAINT [FK_DataImportJob_Entity] FOREIGN KEY([EntityID])
REFERENCES [dbo].[Entity] ([EntityID])
GO
ALTER TABLE [dbo].[DataImportJob] CHECK CONSTRAINT [FK_DataImportJob_Entity]
GO
ALTER TABLE [dbo].[DataImportJob]  WITH CHECK ADD  CONSTRAINT [FK_DataImportJob_ImportJobStatus] FOREIGN KEY([ImportJobStatusID])
REFERENCES [dbo].[ImportJobStatus] ([ImportJobStatusID])
GO
ALTER TABLE [dbo].[DataImportJob] CHECK CONSTRAINT [FK_DataImportJob_ImportJobStatus]
GO
ALTER TABLE [dbo].[ImportAllocationsJobData]  WITH CHECK ADD  CONSTRAINT [FK_ImportAllocationsJobData_DataImportJob] FOREIGN KEY([ImportJobID])
REFERENCES [dbo].[DataImportJob] ([ImportJobID])
GO
ALTER TABLE [dbo].[ImportAllocationsJobData] CHECK CONSTRAINT [FK_ImportAllocationsJobData_DataImportJob]
GO
ALTER TABLE [dbo].[ImportJobsJobData]  WITH CHECK ADD  CONSTRAINT [FK_ImportJobsJobData_DataImportJob] FOREIGN KEY([ImportJobID])
REFERENCES [dbo].[DataImportJob] ([ImportJobID])
GO
ALTER TABLE [dbo].[ImportJobsJobData] CHECK CONSTRAINT [FK_ImportJobsJobData_DataImportJob]
GO
ALTER TABLE [dbo].[ImportUserJobData]  WITH CHECK ADD  CONSTRAINT [FK_ImportUserJobData_Importob] FOREIGN KEY([ImportJobID])
REFERENCES [dbo].[DataImportJob] ([ImportJobID])
GO
ALTER TABLE [dbo].[ImportUserJobData] CHECK CONSTRAINT [FK_ImportUserJobData_Importob]
GO
ALTER TABLE [dbo].[UserDataCustomField]  WITH CHECK ADD  CONSTRAINT [FK_UserDataCustomField_ImportUserJobData] FOREIGN KEY([RecordID])
REFERENCES [dbo].[ImportUserJobData] ([RecordID])
GO
ALTER TABLE [dbo].[UserDataCustomField] CHECK CONSTRAINT [FK_UserDataCustomField_ImportUserJobData]
GO



/****** Data for ImportJobStatus and Entity tables    Script Date: 1/21/2020 9:51:56 PM ******/
GO
SET IDENTITY_INSERT [dbo].[Entity] ON 

INSERT [dbo].[Entity] ([EntityID], [EntityName]) VALUES (1, N'Users')
INSERT [dbo].[Entity] ([EntityID], [EntityName]) VALUES (2, N'Jobs')
INSERT [dbo].[Entity] ([EntityID], [EntityName]) VALUES (3, N'Allocations
')
INSERT [dbo].[Entity] ([EntityID], [EntityName]) VALUES (4, N'Tasks')
INSERT [dbo].[Entity] ([EntityID], [EntityName]) VALUES (5, N'Time Entries')
INSERT [dbo].[Entity] ([EntityID], [EntityName]) VALUES (6, N'Divisions')
INSERT [dbo].[Entity] ([EntityID], [EntityName]) VALUES (7, N'Clients')
SET IDENTITY_INSERT [dbo].[Entity] OFF

GO
SET IDENTITY_INSERT [dbo].[ImportJobStatus] ON 
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (1, N'Not Started')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (2, N'Penidng Process')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (3, N'In Process')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (4, N'Completed')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (5, N'Completed With Errors')
GO
INSERT [dbo].[ImportJobStatus] ([ImportJobStatusID], [ImportJobStatus]) VALUES (6, N'Failed')
GO
SET IDENTITY_INSERT [dbo].[ImportJobStatus] OFF
GO
