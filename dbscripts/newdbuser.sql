Use Master
GO

CREATE LOGIN functionsappuser
    WITH PASSWORD = '[REDACTED]'; -- See Lastpass for redacted credentials
GO



Use ctcoreimport
GO

CREATE USER functionsappuser FOR LOGIN functionsappuser
GO

EXEC sp_addrolemember N'db_datawriter', N'functionsappuser'
GO