# Headless Azure Functions

Headless Azure Functions Let a calling client import multiple records into an Application(“the Application”). These functions support the bulk creation of User records in the Application on behalf of the invoking user; allowing a clientto make an initial request (“the Job”), check the progress of the Job, receive an email notification that the Job is complete (“the Notification”), and have a way of getting a list of failed items

# Prerequisites

  - Install [Visual Studio Code](https://code.visualstudio.com/) on one of the supported platforms
  - Install version 2.x of the [Azure Functions Core Tools](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local#v2)
  - Install [Node.js](https://nodejs.org/)

# Project Structure

>IMPORTHANDLER
> | - CreateImportJob
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| | - index.js
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| | - function.json
> | - GetImportJobDetail
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| | - index.js
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| | - function.json
> | - GetImportJobDetailSummary
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| | - index.js
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| | - function.json
> | - GetJobsList
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| | - index.js
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | | - function.json
> | - app_constants.js
> | - azure_storage_queue_manager.js
> | - clicktime_api.js
> | - database_operations.js
> | - email_manager.js
> | - import_job_processor.js
> | - node_modules
> | - host.json
> | - package.json
> | - local.settings.json

## Small Summary of project files 

#### --CreateImportJob/index.js
It's main Azure function which inititate the import process on client request and perform follwoing operations. 
* Validate the user using ClickTimeAPI
* Create Job in DataImportJob
* Save data into [ImportUserJobData] and [UserDataCustomField] tables
* Return JobNumber with status code 201-Created
* Call the async process to push request in Azure Storage Queue

### --GetImportJobDetail/index.js
This function recieve the "jobid" and "status" as query string parameter and return the complete record with API Response/Error Message. "status" is optional parameter to filter records for a particular job.This function perform the following operations.
* Return All records for a job if "status" parameter is not supplied in request
* Return All records with error if "status=error" is passed in querystring
* Return All records which was created successfully if "status=success" is passed in querystring

### --GetImportJobDetailSummary/index.js
This function recieve the "jobid" as query string parameter and return the job summary so user can track the job progress or view the overall summary of a job. Below is the sample response of this function.
```
{
    "Job Number": 1,
    "Job Creation": "2020-01-04T22:06:29.377Z",
    "Job Start Time": "2020-01-04T17:07:02.977Z",
    "Job Completion Time": "2020-01-04T17:07:38.813Z",
    "Total Records In Request": 3,
    "Total Records Processed": 3,
    "Total Processed - Success": 1,
    "Total Processed - Errors": 2
}
```

### --GetJobsList/index.js
This azure function recieve "status" as optional parameter and return List of the job created by a user along with summary of the job.Below is the values for "status" parameter.
| Status | Response |
| ------ | ------ |
| complete | Return all jobs created by the user with "Completed" status  |
| processing | Return all jobs created by the user which have "In Process" status  |
| notstarted | Return all jobs created by the user which have "Not Started" status  |

Below is sample response for this function.

```
[
    {
        "Job Number": 1,
        "Job Status": "Completed",
        "Job Creation": "2020-01-04T22:06:29.377Z",
        "Job Start Time": "2020-01-04T17:07:02.977Z",
        "Job Completion Time": "2020-01-04T17:07:38.813Z",
        "Total Records In Request": 3,
        "Total Records Processed": 3,
        "Total Processed - Success": 0,
        "Total Processed - Errors": 3
    },
    {
        "Job Number": 2,
        "Job Status": "Completed",
        "Job Creation": "2020-01-04T22:12:03.350Z",
        "Job Start Time": "2020-01-04T17:12:20.770Z",
        "Job Completion Time": "2020-01-04T17:12:25.863Z",
        "Total Records In Request": 3,
        "Total Records Processed": 3,
        "Total Processed - Success": 1,
        "Total Processed - Errors": 2
    }
]
```

###### Note: You need to pass the authentication token as "Authorization" header in order to consume this Azure Function.

### --app_constants.js
This file has all constants used in different files of the application, below are some constants from the file

```
ClickTimeAPI: {
        Protocol: 'https://',
        Host: 'api.clicktime.com',
        /** List of ClickTime API Endpoints */
        Endpoints:{
            GetTimeReport:'/v2/reports/time',
            GetUsersData: '/v2/users',
            GetJobsData: '/v2/jobs',
            ExecuteBatchRequest: '/v2/$batch',
            GetUserInfo: '/v2/me',
            GetUserCompanyInfo: '/v2/company',
            CreateUser:'/v2/users'
        },
        BatchRequestLimit:49
    },
    /** Entity values exist in Entity table */
    Entity:{
        Users: 1,
        Jobs: 2
    }
```

### --azure_storage_queue_manager.js
This module has all functions which deals with Azure Storage Queue, It's implement the Azure Storage Queue SDK and fo the following operation.

* Check for queue existance for a user
* Push the import job request to queue if it's already exsist as queue existance indicate that anotehr jon for this user is already in process
* Call the method to ProcessImportJob
* If queue dose not exist it's check the datbase to check how many jobs are currently in process
* If in process jobs count is less than 20 than its create a new queue for the user
* Push message to newly created Queue

###### All other methods which deals with Azure Storage Queue like update messsage, delete message, delete queue are implemented in this file.

### --clicktime_api.js
This module consume the all click time api methods which are required in this Azure Function App.This module also have method to create batch body and parser to extract results from it.

### --database_operations.js
This module implements "tedious" package to intreact with Azure SQL Database, This module has all methods used accross the appliction for inreacting with database.

### -- email_manager.js
This module implement "nodemailer" to send emails form the application.

### -- import_job_processor.js
This module process the import job by consuming methods from "azure_storage_queue_manager" module.It's perform the follwoing operations by calling methods from different modules.

* Itterate the queue to process all job requests from a user
* Update the status for each record in a job
* Update the sttaus of a job
* Send completion email to user
* Delet the queue when all jobs has been processed 
* Callback the Azure Queue Manager after destroyng a queue so Azure Queue Manager can check for any pending request in database for anyother user, push it into the queue and call "import_job_processor" again to process newly created queue.

### -- package.json
This file contain package information which are required for the application. On deployment azure automatically install those packages using this file.

### -- local.settings.json
All Azure Enviornment variables are defined in this file.


## Deployment
* In Visual Studio Code, press F1 to open the command palette. In the command palette, search for and select Azure Functions: Deploy to function app....

* If not signed-in, you are prompted to Sign in to Azure. You can also Create a free Azure account. After successful sign in from the browser, go back to Visual Studio Code.

* If you have multiple subscriptions, Select a subscription for the function app, then choose + Create New Function App in Azure.

* Type a globally unique name that identifies your function app and press Enter. Valid characters for a function app name are a-z, 0-9, and -.

When you press Enter, the following Azure resources are created in your subscription:

1-  Resource group: Contains all of the created Azure resources. The name is based on your function app name.
2- Storage account: A standard Storage account is created with a unique name that is based on your function app name.
3- Hosting plan: A consumption plan is created in the West US region to host your serverless function app.
4- Function app: Your project is deployed to and runs in this new function app.

A notification is displayed after your function app is created and the deployment package is applied. Select View Output in this notification to view the creation and deployment results, including the Azure resources that you created.

* Back in the Azure: Functions area, expand the new function app under your subscription. Expand Functions, right-click HttpTrigger, and then choose Copy function URL.
