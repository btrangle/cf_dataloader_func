// import Entity Types constant from app_constants module
const tedious = require('./tedious_implementation');
// import isNullOrEmpty method to check string is Empty or Null
const isNullOrEmpty = require('../general_utilities').isNullOrEmpty;
// import general_operations module
const generalOperations = require('./general_operations');
module.exports = {
  /**
  * Save User's Data into ImportJob database for tracking progress of job
  * @param integer importJobID - ImportJob Identifier from Database
  * @param array usersData - Array of User Objects which need to be created
  */
  saveJobsDataToDatabase: async function ({ importJobID, jobsData, entityTypeID }) {
    return new Promise(async (resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        let isSuccessfullyCompleted = true;
        if(connectionPool !== null) {
          for (const jobRecord of jobsData) {
            await insertIntoImportJobsJobDataTable({
              importJobID: importJobID,
              jobObjectToSave: jobRecord,
              entityTypeID: entityTypeID,
              connectionPool: connectionPool
            }).catch(error => {
              isSuccessfullyCompleted = false;
              console.log(error);
            });
          }
        } else{
          isSuccessfullyCompleted = false;
        }
        tedious.drainPool(connectionPool);
        resolve(isSuccessfullyCompleted);
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  }
};

/**
* Save Job's Data into ImportJob database for tracking progress of job
* @param integer importJobID - ImportJob Identifier fom Database
* @param Object jobObjectToSave - job Object
* @param Object connectionPool -Tedious Connection Pool
*/
async function insertIntoImportJobsJobDataTable ({ importJobID, jobObjectToSave, entityTypeID, connectionPool }) {
  return new Promise((resolve, reject) => {
    try {
      let insertedRecordID = 0;
      let createdSuccessFully = null;
      let importJobComments = '';
      // acquire a connection from pool
      connectionPool.acquire(function (err, connection) {
        if (err) {
          console.error(err.message);
          // release the connection back to the pool when finished
          connection.release();
          reject(err.message);
        } else {
          if (isNullOrEmpty(jobObjectToSave.ClientID) || isNullOrEmpty(jobObjectToSave.Name) || isNullOrEmpty(jobObjectToSave.JobNumber)) {
            importJobComments = 'Missing Required Fields';
            createdSuccessFully = false;
          }
          const request = new tedious.Request(`INSERT INTO [dbo].[Jobs]
                    ([ImportJobID]
                    ,[AccountingPackageID]
                    ,[BillingRate]
                    ,[ClientID]
                    ,[IsActive]
                    ,[IsBillable]
                    ,[JobNumber]
                    ,[Name]
                    ,[Notes]
                    ,[ProjectManagerID]
                    ,[SecondaryBillingRateMode]
                    ,[TimeRequiresApproval]
                    ,[UseCompanyBillingRate]
                    ,[CreatedSuccessfully]
                    ,[ImportJobComments]) OUTPUT INSERTED.RecordID
                    VALUES
                    (@ImportJobID
                    ,@AccountingPackageID
                    ,@BillingRate
                    ,@ClientID
                    ,@IsActive
                    ,@IsBillable
                    ,@JobNumber
                    ,@Name
                    ,@Notes
                    ,@ProjectManagerID
                    ,@SecondaryBillingRateMode
                    ,@TimeRequiresApproval
                    ,@UseCompanyBillingRate
                    ,@CreatedSuccessfully
                    ,@ImportJobComments)`, function (err, rowCount) {
            if (err) {
              console.error(err.message);
            }
          });
          request.addParameter('ImportJobID', tedious.Types.Int, importJobID);
          request.addParameter('AccountingPackageID', tedious.Types.NVarChar, jobObjectToSave.AccountingPackageID);
          request.addParameter('BillingRate', tedious.Types.Money, jobObjectToSave.BillingRate);
          request.addParameter('ClientID', tedious.Types.NVarChar, jobObjectToSave.ClientID);
          request.addParameter('IsActive', tedious.Types.Bit, jobObjectToSave.IsActive);
          request.addParameter('IsBillable', tedious.Types.Bit, jobObjectToSave.IsBillable);
          request.addParameter('JobNumber', tedious.Types.NVarChar, jobObjectToSave.JobNumber);
          request.addParameter('Name', tedious.Types.NVarChar, jobObjectToSave.Name);
          request.addParameter('Notes', tedious.Types.NVarChar, jobObjectToSave.Notes);
          request.addParameter('ProjectManagerID', tedious.Types.NVarChar, jobObjectToSave.ProjectManagerID);
          request.addParameter('SecondaryBillingRateMode', tedious.Types.NVarChar, jobObjectToSave.SecondaryBillingRateMode);
          request.addParameter('TimeRequiresApproval', tedious.Types.Bit, jobObjectToSave.TimeRequiresApproval);
          request.addParameter('UseCompanyBillingRate', tedious.Types.Bit, jobObjectToSave.UseCompanyBillingRate);
          request.addParameter('CreatedSuccessfully', tedious.Types.Bit, createdSuccessFully);
          request.addParameter('ImportJobComments', tedious.Types.NVarChar, importJobComments);
          request.on('row', function (columns) {
            columns.forEach(function (column) {
              if (column.value === null) {
                console.log('NULL');
              } else {
                insertedRecordID = column.value;
                console.log('Record ID of inserted item is ' + column.value);
              }
            });
          });
          request.on('requestCompleted', async function () {
            // release the connection back to the pool when finished
            connection.release();
            if(insertedRecordID !== 0 && jobObjectToSave.hasOwnProperty('CustomFields') && jobObjectToSave.CustomFields) {
              const customFieldNames = Object.keys(jobObjectToSave.CustomFields);
              for (const customFieldName of customFieldNames) {
                var customFieldValue = JSON.stringify(jobObjectToSave.CustomFields[customFieldName]);
                await generalOperations.insertCustomFieldsData({
                  recordID: insertedRecordID,
                  entityTypeID: entityTypeID,
                  customFieldName: customFieldName,
                  customFieldValue: customFieldValue,
                  connectionPool: connectionPool
                }).catch(error => {
                  console.log(error);
                });
              }
            }
            resolve();
          });
          connection.execSql(request);
        }
      });
    } catch(ex) {
      console.error(ex.message);
      reject(ex.message);
    }
  });
};
