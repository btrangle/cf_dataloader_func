// import Entity Types constant from app_constants module
const tedious = require('./tedious_implementation');
// import isNullOrEmpty method to check string is Empty or Null
const isNullOrEmpty = require('../general_utilities').isNullOrEmpty;
// import general_operations module
const generalOperations = require('./general_operations');
module.exports = {
  /**
  * Save Division's Data into ImportJob database for tracking progress of job
  * @param integer importJobID - ImportJob Identifier from Database
  * @param array divisionsData - Array of Division Objects which need to be created
  */
  saveDivisionsDataToDatabase: async function ({ importJobID, entityTypeID, divisionsData }) {
    return new Promise(async (resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        let isSuccessfullyCompleted = true;
        if(connectionPool !== null) {
          for (const divisionRecord of divisionsData) {
            await insertIntoImportDivisionsJobDataTable({
              importJobID: importJobID,
              divisionObjectToSave: divisionRecord,
              entityTypeID: entityTypeID,
              connectionPool: connectionPool
            }).catch(error => {
              isSuccessfullyCompleted = false;
              console.log(error);
            });
          }
        } else{
          isSuccessfullyCompleted = false;
        }
        tedious.drainPool(connectionPool);
        resolve(isSuccessfullyCompleted);
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  }
};

/**
* Save Division's Data into ImportJob database for tracking progress of job
* @param integer importJobID - ImportJob Identifier fom Database
* @param Object divisionObjectToSave - Divisions Object
* @param Object connectionPool -Tedious Connection Pool
*/
async function insertIntoImportDivisionsJobDataTable ({ importJobID, divisionObjectToSave, entityTypeID, connectionPool }) {
  return new Promise((resolve, reject) => {
    try {
      let insertedRecordID = 0;
      let createdSuccessFully = null;
      let importJobComments = '';
      // acquire a connection from pool
      connectionPool.acquire(function (err, connection) {
        if (err) {
          console.error(err.message);
          // release the connection back to the pool when finished
          connection.release();
          reject(err.message);
        } else {
          if (isNullOrEmpty(divisionObjectToSave.Name)) {
            importJobComments = 'Missing Required Field';
            createdSuccessFully = false;
          }
          const request = new tedious.Request(`INSERT INTO [dbo].[Divisions]
                    ([ImportJobID]
                    ,[AccountingPackageID]
                    ,[IsActive]
                    ,[Name]
                    ,[Notes]
                    ,[CreatedSuccessfully]
                    ,[ImportJobComments]) OUTPUT INSERTED.RecordID
                    VALUES
                    (@ImportJobID
                    ,@AccountingPackageID
                    ,@IsActive
                    ,@Name
                    ,@Notes
                    ,@CreatedSuccessfully
                    ,@ImportJobComments)`, function (err, rowCount) {
            if (err) {
              console.error(err.message);
            }
          });
          request.addParameter('ImportJobID', tedious.Types.Int, importJobID);
          request.addParameter('AccountingPackageID', tedious.Types.NVarChar, divisionObjectToSave.AccountingPackageID);
          request.addParameter('IsActive', tedious.Types.Bit, divisionObjectToSave.IsActive);
          request.addParameter('Name', tedious.Types.NVarChar, divisionObjectToSave.Name);
          request.addParameter('Notes', tedious.Types.NVarChar, divisionObjectToSave.Notes);
          request.addParameter('CreatedSuccessfully', tedious.Types.Bit, createdSuccessFully);
          request.addParameter('ImportJobComments', tedious.Types.NVarChar, importJobComments);
          request.on('row', function (columns) {
            columns.forEach(function (column) {
              if (column.value === null) {
                console.log('NULL');
              } else {
                insertedRecordID = column.value;
                console.log('Record ID of inserted item is ' + column.value);
              }
            });
          });
          request.on('requestCompleted', async function () {
            // release the connection back to the pool when finished
            connection.release();
            if(insertedRecordID !== 0 && divisionObjectToSave.hasOwnProperty('CustomFields') && divisionObjectToSave.CustomFields) {
              const customFieldNames = Object.keys(divisionObjectToSave.CustomFields);
              for (const customFieldName of customFieldNames) {
                if(divisionObjectToSave.CustomFields[customFieldName] != null && divisionObjectToSave.CustomFields[customFieldName] !== 'null') {
                  var customFieldValue = JSON.stringify(divisionObjectToSave.CustomFields[customFieldName]);
                  await generalOperations.insertCustomFieldsData({
                    recordID: insertedRecordID,
                    entityTypeID: entityTypeID,
                    customFieldName: customFieldName,
                    customFieldValue: customFieldValue,
                    connectionPool: connectionPool
                  }).catch(error => {
                    console.log(error);
                  });
                }
              }
            }
            resolve();
          });
          connection.execSql(request);
        }
      });
    } catch(ex) {
      console.error(ex.message);
      reject(ex.message);
    }
  });
};
