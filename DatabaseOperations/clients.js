// import Entity Types constant from app_constants module
const tedious = require('./tedious_implementation');
// import isNullOrEmpty method to check string is Empty or Null
const isNullOrEmpty = require('../general_utilities').isNullOrEmpty;
// import general_operations module
const generalOperations = require('./general_operations');
module.exports = {
  /**
  * Save Client's Data into ImportJob database for tracking progress of job
  * @param integer importJobID - ImportJob Identifier from Database
  * @param array clientsData - Array of Client Objects which need to be created
  */
  saveClientsDataToDatabase: async function ({ importJobID, entityTypeID, clientsData }) {
    return new Promise(async (resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        let isSuccessfullyCompleted = true;
        if(connectionPool !== null) {
          for (const clientRecord of clientsData) {
            await insertIntoImportClientsJobDataTable({
              importJobID: importJobID,
              clientObjectToSave: clientRecord,
              entityTypeID: entityTypeID,
              connectionPool: connectionPool
            }).catch(error => {
              isSuccessfullyCompleted = false;
              console.log(error);
            });
          }
        } else{
          isSuccessfullyCompleted = false;
        }
        tedious.drainPool(connectionPool);
        resolve(isSuccessfullyCompleted);
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  }
};

/**
* Save Client's Data into ImportJob database for tracking progress of job
* @param integer importJobID - ImportJob Identifier fom Database
* @param Object clientObjectToSave - Client Object
* @param Object connectionPool -Tedious Connection Pool
*/
async function insertIntoImportClientsJobDataTable ({ importJobID, clientObjectToSave, entityTypeID, connectionPool }) {
  return new Promise((resolve, reject) => {
    try {
      let insertedRecordID = 0;
      let createdSuccessFully = null;
      let importJobComments = '';
      // acquire a connection from pool
      connectionPool.acquire(function (err, connection) {
        if (err) {
          console.error(err.message);
          // release the connection back to the pool when finished
          connection.release();
          reject(err.message);
        } else {
          if (isNullOrEmpty(clientObjectToSave.Name) || isNullOrEmpty(clientObjectToSave.ShortName)) {
            importJobComments = 'Missing Required Field';
            createdSuccessFully = false;
          }
          const request = new tedious.Request(`INSERT INTO [dbo].[Clients]
                    ([ImportJobID]
                    ,[AccountingPackageID]
                    ,[BillingRate]
                    ,[ClientNumber]
                    ,[IsActive]
                    ,[Name]
                    ,[Notes]
                    ,[SecondaryBillingRateMode]
                    ,[ShortName]
                    ,[CreatedSuccessfully]
                    ,[ImportJobComments]) OUTPUT INSERTED.RecordID
                    VALUES
                    (@ImportJobID
                    ,@AccountingPackageID
                    ,@BillingRate
                    ,@ClientNumber
                    ,@IsActive
                    ,@Name
                    ,@Notes
                    ,@SecondaryBillingRateMode
                    ,@ShortName
                    ,@CreatedSuccessfully
                    ,@ImportJobComments)`, function (err, rowCount) {
            if (err) {
              console.error(err.message);
            }
          });
          request.addParameter('ImportJobID', tedious.Types.Int, importJobID);
          request.addParameter('AccountingPackageID', tedious.Types.NVarChar, clientObjectToSave.AccountingPackageID);
          request.addParameter('BillingRate', tedious.Types.Money, clientObjectToSave.BillingRate);
          request.addParameter('ClientNumber', tedious.Types.NVarChar, clientObjectToSave.ClientNumber);
          request.addParameter('IsActive', tedious.Types.Bit, clientObjectToSave.IsActive);
          request.addParameter('Name', tedious.Types.NVarChar, clientObjectToSave.Name);
          request.addParameter('Notes', tedious.Types.NVarChar, clientObjectToSave.Notes);
          request.addParameter('SecondaryBillingRateMode', tedious.Types.NVarChar, clientObjectToSave.SecondaryBillingRateMode);
          request.addParameter('ShortName', tedious.Types.NVarChar, clientObjectToSave.ShortName);
          request.addParameter('CreatedSuccessfully', tedious.Types.Bit, createdSuccessFully);
          request.addParameter('ImportJobComments', tedious.Types.NVarChar, importJobComments);
          request.on('row', function (columns) {
            columns.forEach(function (column) {
              if (column.value === null) {
                console.log('NULL');
              } else {
                insertedRecordID = column.value;
                console.log('Record ID of inserted item is ' + column.value);
              }
            });
          });
          request.on('requestCompleted', async function () {
            // release the connection back to the pool when finished
            connection.release();
            if(insertedRecordID !== 0 && clientObjectToSave.hasOwnProperty('CustomFields') && clientObjectToSave.CustomFields) {
              const customFieldNames = Object.keys(clientObjectToSave.CustomFields);
              for (const customFieldName of customFieldNames) {
                if(clientObjectToSave.CustomFields[customFieldName] != null && clientObjectToSave.CustomFields[customFieldName] !== 'null') {
                  var customFieldValue = JSON.stringify(clientObjectToSave.CustomFields[customFieldName]);
                  await generalOperations.insertCustomFieldsData({
                    recordID: insertedRecordID,
                    entityTypeID: entityTypeID,
                    customFieldName: customFieldName,
                    customFieldValue: customFieldValue,
                    connectionPool: connectionPool
                  }).catch(error => {
                    console.log(error);
                  });
                }
              }
            }
            resolve();
          });
          connection.execSql(request);
        }
      });
    } catch(ex) {
      console.error(ex.message);
      reject(ex.message);
    }
  });
};
