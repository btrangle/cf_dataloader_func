// import isNullOrEmpty method to check string is Empty or Null
const isNullOrEmpty = require('../general_utilities').isNullOrEmpty;
// import Entity Types constant from app_constants module
const tedious = require('./tedious_implementation');
module.exports = {
  /**
  * Save User's Data into ImportJob database for tracking progress of job
  * @param integer importJobID - ImportJob Identifier from Database
  * @param array usersData - Array of User Objects which need to be created
  */
  saveAllocationsDataToDatabase: async function ({ importJobID, allocationsData }) {
    return new Promise(async (resolve, reject) => {
    // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        let isSuccessfullyCompleted = true;
        if(connectionPool !== null) {
          for (const allocationRecord of allocationsData) {
            await insertIntoImportAllocationsJobDataTable({
              importJobID: importJobID,
              allocationRecord: allocationRecord,
              connectionPool: connectionPool
            }).catch(error => {
              console.error(error.message);
            });
          }
        } else{
          isSuccessfullyCompleted = false;
        }
        tedious.drainPool(connectionPool);
        resolve(isSuccessfullyCompleted);
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  }
};
/**
* Save Allocation's Data into ImportJob database for tracking progress of job
* @param integer importJobID - ImportJob Identifier fom Database
* @param Object allocationRecord - Allocation Object
* @param Object connectionPool -Tedious Connection Pool
*/
async function insertIntoImportAllocationsJobDataTable ({ importJobID, allocationRecord, connectionPool }) {
  return new Promise((resolve, reject) => {
    try {
      let createdSuccessFully = null;
      let importJobComments = '';
      // acquire a connection from pool
      connectionPool.acquire(function (err, connection) {
        if (err) {
          console.error(err.message);
          // release the connection back to the pool when finished
          connection.release();
          reject(err.message);
        } else {
          if (isNullOrEmpty(allocationRecord.EstHours) || isNullOrEmpty(allocationRecord.JobID) || isNullOrEmpty(allocationRecord.Month) || isNullOrEmpty(allocationRecord.UserID)) {
            importJobComments = 'Missing Required Fields';
            createdSuccessFully = false;
          }
          const request = new tedious.Request(`INSERT INTO [dbo].[Allocations]
                  ([ImportJobID]
                  ,[EstHours]
                  ,[JobID]
                  ,[Month]
                  ,[UserID]
                  ,[CreatedSuccessfully]
                  ,[ImportJobComments]) OUTPUT INSERTED.RecordID
                      VALUES
                      (@ImportJobID
                      ,@EstHours
                      ,@JobID
                      ,@Month
                      ,@UserID
                      ,@CreatedSuccessfully
                      ,@ImportJobComments)`, function (err, rowCount) {
            if (err) {
              console.error(err.message);
            }
          });
          request.addParameter('ImportJobID', tedious.Types.Int, importJobID);
          request.addParameter('EstHours', tedious.Types.Int, allocationRecord.EstHours);
          request.addParameter('JobID', tedious.Types.NVarChar, allocationRecord.JobID);
          request.addParameter('Month', tedious.Types.NVarChar, allocationRecord.Month);
          request.addParameter('UserID', tedious.Types.NVarChar, allocationRecord.UserID);
          request.addParameter('CreatedSuccessfully', tedious.Types.Bit, createdSuccessFully);
          request.addParameter('ImportJobComments', tedious.Types.NVarChar, importJobComments);
          request.on('row', function (columns) {
            columns.forEach(function (column) {
              if (column.value === null) {
                console.log('NULL');
              } else {
                console.log('Record ID of inserted item is ' + column.value);
              }
            });
          });
          request.on('requestCompleted', async function () {
            // release the connection back to the pool when finished
            connection.release();
            resolve();
          });
          connection.execSql(request);
        }
      });
    } catch(ex) {
      console.error(ex.message);
      reject(ex.message);
    }
  });
};
