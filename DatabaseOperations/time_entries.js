// import Entity Types constant from app_constants module
const tedious = require('./tedious_implementation');
// import isNullOrEmpty method to check string is Empty or Null
const isNullOrEmpty = require('../general_utilities').isNullOrEmpty;
// import general_operations module
const generalOperations = require('./general_operations');
module.exports = {
  /**
  * Save User's Data into ImportJob database for tracking progress of job
  * @param integer importJobID - ImportJob Identifier from Database
  * @param array usersData - Array of User Objects which need to be created
  */
  saveTimeEntriesDataToDatabase: async function ({ importJobID, timeEntriesData, entityTypeID }) {
    return new Promise(async (resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        let isSuccessfullyCompleted = true;
        if(connectionPool !== null) {
          for (const timeEntrieRecord of timeEntriesData) {
            await insertIntoImportTimeEntriesJobDataTable({
              importJobID: importJobID,
              timeEntryObjectToSave: timeEntrieRecord,
              entityTypeID: entityTypeID,
              connectionPool: connectionPool
            }).catch(error => {
              isSuccessfullyCompleted = false;
              console.log(error);
            });
          }
        } else{
          isSuccessfullyCompleted = false;
        }
        tedious.drainPool(connectionPool);
        resolve(isSuccessfullyCompleted);
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  }
};

/**
* Save Job's Data into ImportJob database for tracking progress of job
* @param integer importJobID - ImportJob Identifier fom Database
* @param Object taskObjectToSave - task Object
* @param Object connectionPool -Tedious Connection Pool
*/
async function insertIntoImportTimeEntriesJobDataTable ({ importJobID, timeEntryObjectToSave, entityTypeID, connectionPool }) {
  return new Promise((resolve, reject) => {
    try {
      let insertedRecordID = 0;
      let createdSuccessFully = null;
      let importJobComments = '';
      // acquire a connection from pool
      connectionPool.acquire(function (err, connection) {
        if (err) {
          console.error(err.message);
          // release the connection back to the pool when finished
          connection.release();
          reject(err.message);
        } else {
          if (isNullOrEmpty(timeEntryObjectToSave.UserID) || isNullOrEmpty(timeEntryObjectToSave.JobID) || isNullOrEmpty(timeEntryObjectToSave.TaskID)) {
            importJobComments = 'Missing Required Fields';
            createdSuccessFully = false;
          }
          const request = new tedious.Request(`INSERT INTO [dbo].[TimeEntries]
                    ([ImportJobID]
                    ,[BillingRate]
                    ,[BreakTime]
                    ,[Comment]
                    ,[CostRate]
                    ,[Date]
                    ,[EndTime]
                    ,[Hours]
                    ,[JobID]
                    ,[StartTime]
                    ,[TaskID]
                    ,[UserID]
                    ,[CreatedSuccessfully]
                    ,[ImportJobComments]) OUTPUT INSERTED.RecordID
                    VALUES
                    (@ImportJobID
                    ,@BillingRate
                    ,@BreakTime
                    ,@Comment
                    ,@CostRate
                    ,@Date
                    ,@EndTime
                    ,@Hours
                    ,@JobID
                    ,@StartTime
                    ,@TaskID
                    ,@UserID
                    ,@CreatedSuccessfully
                    ,@ImportJobComments)`, function (err, rowCount) {
            if (err) {
              console.error(err.message);
            }
          });
          request.addParameter('ImportJobID', tedious.Types.Int, importJobID);
          request.addParameter('BillingRate', tedious.Types.Money, timeEntryObjectToSave.BillingRate);
          request.addParameter('BreakTime', tedious.Types.NVarChar, timeEntryObjectToSave.BreakTime);
          request.addParameter('Comment', tedious.Types.NVarChar, timeEntryObjectToSave.Comment);
          request.addParameter('CostRate', tedious.Types.Money, timeEntryObjectToSave.CostRate);
          request.addParameter('Date', tedious.Types.NVarChar, timeEntryObjectToSave.Date);
          request.addParameter('EndTime', tedious.Types.NVarChar, timeEntryObjectToSave.EndTime);
          request.addParameter('Hours', tedious.Types.NVarChar, timeEntryObjectToSave.Hours);
          request.addParameter('JobID', tedious.Types.NVarChar, timeEntryObjectToSave.JobID);
          request.addParameter('StartTime', tedious.Types.NVarChar, timeEntryObjectToSave.StartTime);
          request.addParameter('TaskID', tedious.Types.NVarChar, timeEntryObjectToSave.TaskID);
          request.addParameter('UserID', tedious.Types.NVarChar, timeEntryObjectToSave.UserID);
          request.addParameter('CreatedSuccessfully', tedious.Types.Bit, createdSuccessFully);
          request.addParameter('ImportJobComments', tedious.Types.NVarChar, importJobComments);
          request.on('row', function (columns) {
            columns.forEach(function (column) {
              if (column.value === null) {
                console.log('NULL');
              } else {
                insertedRecordID = column.value;
                console.log('Record ID of inserted item is ' + column.value);
              }
            });
          });
          request.on('requestCompleted', async function () {
            // release the connection back to the pool when finished
            connection.release();
            if(insertedRecordID !== 0 && timeEntryObjectToSave.hasOwnProperty('CustomFields') && timeEntryObjectToSave.CustomFields) {
              const customFieldNames = Object.keys(timeEntryObjectToSave.CustomFields);
              for (const customFieldName of customFieldNames) {
                var customFieldValue = JSON.stringify(timeEntryObjectToSave.CustomFields[customFieldName]);
                await generalOperations.insertCustomFieldsData({
                  recordID: insertedRecordID,
                  entityTypeID: entityTypeID,
                  customFieldName: customFieldName,
                  customFieldValue: customFieldValue,
                  connectionPool: connectionPool
                }).catch(error => {
                  console.log(error);
                });
              }
            }
            resolve();
          });
          connection.execSql(request);
        }
      });
    } catch(ex) {
      console.error(ex.message);
      reject(ex.message);
    }
  });
};
