module.exports = {
  Types: require('tedious').TYPES,
  Request: require('tedious').Request,
  getConnectionPool: function () {
    try{
      // import tedious to interact with instances of Microsoft's SQL Serve
      const TediousConnectionPool = require('tedious-connection-pool');
      // SQL Database Configiration using Envoirnment Variables
      const connectionConfig = {
        userName: process.env['SQLUserName'],
        password: process.env['SQLPassword'],
        server: process.env['SQLServerName'],
        options: {
          database: process.env['DatabaseName'],
          encrypt: true,
          packetSize: 65536
        }
      };
        // SQL Connection pool config
      const poolConfig = {
        min: process.env['MinSQLConnection'],
        max: process.env['MaxSQLConnection'],
        log: true
      };
      // create the tedious-connection pool
      var pool = new TediousConnectionPool(poolConfig, connectionConfig);
      pool.on('error', function (err) {
        console.error(err);
      });
      return pool;
    }catch(ex) {
      return null;
    }
  },
  drainPool: function (connectionPool) {
    try{
      if (connectionPool) {
        connectionPool.drain();
      }
    }catch(ex) {
      console.error('Exception occured, while trying to get Connection Pool');
      console.error(ex.message);
    }
  }
};
