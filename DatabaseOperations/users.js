// import Entity Types constant from app_constants module
const tedious = require('./tedious_implementation');
// import isNullOrEmpty method to check string is Empty or Null
const isNullOrEmpty = require('../general_utilities').isNullOrEmpty;
// import general_operations module
const generalOperations = require('./general_operations');
module.exports = {
  /**
  * Save User's Data into ImportJob database for tracking progress of job
  * @param integer importJobID - ImportJob Identifier from Database
  * @param array usersData - Array of User Objects which need to be created
  */
  saveUsersDataToDatabase: async function ({ importJobID, entityTypeID, usersData }) {
    return new Promise(async (resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        let isSuccessfullyCompleted = true;
        if(connectionPool !== null) {
          for (const userRecord of usersData) {
            await insertIntoImportUserJobDataTable({
              importJobID: importJobID,
              userObjectToSave: userRecord,
              entityTypeID: entityTypeID,
              connectionPool: connectionPool
            }).catch(error => {
              isSuccessfullyCompleted = false;
              console.log(error);
            });
          }
        } else{
          isSuccessfullyCompleted = false;
        }
        tedious.drainPool(connectionPool);
        resolve(isSuccessfullyCompleted);
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  }
};

/**
* Save User's Data into ImportJob database for tracking progress of job
* @param integer importJobID - ImportJob Identifier fom Database
* @param Object userObjectToSave - User Object
* @param Object connectionPool -Tedious Connection Pool
*/
async function insertIntoImportUserJobDataTable ({ importJobID, userObjectToSave, entityTypeID, connectionPool }) {
  return new Promise((resolve, reject) => {
    try {
      let insertedRecordID = 0;
      let createdSuccessFully = null;
      let importJobComments = '';
      // acquire a connection from pool
      connectionPool.acquire(function (err, connection) {
        if (err) {
          console.error(err.message);
          // release the connection back to the pool when finished
          connection.release();
          reject(err.message);
        } else {
          if (isNullOrEmpty(userObjectToSave.Name) || isNullOrEmpty(userObjectToSave.Email) || isNullOrEmpty(userObjectToSave.SecurityLevel)) {
            importJobComments = 'Missing Required Fields';
            createdSuccessFully = false;
          }
          const request = new tedious.Request(`INSERT INTO [dbo].[Users]
                    ([ImportJobID],[AccountingPackageID],[AllowIncompleteTimesheetSubmission],[BillingRate]
                    ,[CostRate]
                    ,[DefaultExpenseTypeID]
                    ,[DefaultPaymentTypeID]
                    ,[DefaultTaskID]
                    ,[DivisionID]
                    ,[Email]
                    ,[EmployeeNumber]
                    ,[EmploymentTypeID]
                    ,[EnableBreakTime]
                    ,[EndDate]
                    ,[ExpenseApproverID]
                    ,[GDPRConsentStatus]
                    ,[IsActive]
                    ,[MinimumTimeHours]
                    ,[MinimumTimePeriod]
                    ,[Name]
                    ,[Notes]
                    ,[PayrollType]
                    ,[PreferredTimeEntryView]
                    ,[PreferredTimeFormat]
                    ,[RequireComments]
                    ,[RequireStartEndTime]
                    ,[RequireStopwatch]
                    ,[Role]
                    ,[SecurityLevel]
                    ,[SkipWeekend]
                    ,[StartDate]
                    ,[SubjectToExpenseApproval]
                    ,[SubjectToTimesheetApproval]
                    ,[SubjectToTimesheetCompletion]
                    ,[TimeOffApproverID]
                    ,[TimesheetApproverID]
                    ,[UseCompanyBillingRate]
                    ,[CreatedSuccessfully]
                    ,[ImportJobComments]) OUTPUT INSERTED.RecordID
                    VALUES
                    (@ImportJobID
                    ,@AccountingPackageID
                    ,@AllowIncompleteTimesheetSubmission
                    ,@BillingRate
                    ,@CostRate
                    ,@DefaultExpenseTypeID
                    ,@DefaultPaymentTypeID
                    ,@DefaultTaskID
                    ,@DivisionID
                    ,@Email
                    ,@EmployeeNumber
                    ,@EmploymentTypeID
                    ,@EnableBreakTime
                    ,@EndDate
                    ,@ExpenseApproverID
                    ,@GDPRConsentStatus
                    ,@IsActive
                    ,@MinimumTimeHours
                    ,@MinimumTimePeriod
                    ,@Name
                    ,@Notes
                    ,@PayrollType
                    ,@PreferredTimeEntryView
                    ,@PreferredTimeFormat
                    ,@RequireComments
                    ,@RequireStartEndTime
                    ,@RequireStopwatch
                    ,@Role
                    ,@SecurityLevel
                    ,@SkipWeekend
                    ,@StartDate
                    ,@SubjectToExpenseApproval
                    ,@SubjectToTimesheetApproval
                    ,@SubjectToTimesheetCompletion
                    ,@TimeOffApproverID
                    ,@TimesheetApproverID
                    ,@UseCompanyBillingRate
                    ,@CreatedSuccessfully
                    ,@ImportJobComments)`, function (err, rowCount) {
            if (err) {
              console.error(err.message);
            }
          });
          request.addParameter('ImportJobID', tedious.Types.Int, importJobID);
          request.addParameter('AccountingPackageID', tedious.Types.NVarChar, userObjectToSave.AccountingPackageID);
          request.addParameter('AllowIncompleteTimesheetSubmission', tedious.Types.Bit, userObjectToSave.AllowIncompleteTimesheetSubmission);
          request.addParameter('BillingRate', tedious.Types.Money, userObjectToSave.BillingRate);
          request.addParameter('CostRate', tedious.Types.Money, userObjectToSave.CostRate);
          request.addParameter('DefaultExpenseTypeID', tedious.Types.NVarChar, userObjectToSave.DefaultExpenseTypeID);
          request.addParameter('DefaultPaymentTypeID', tedious.Types.NVarChar, userObjectToSave.DefaultPaymentTypeID);
          request.addParameter('DefaultTaskID', tedious.Types.NVarChar, userObjectToSave.DefaultTaskID);
          request.addParameter('DivisionID', tedious.Types.NVarChar, userObjectToSave.DivisionID);
          request.addParameter('Email', tedious.Types.NVarChar, userObjectToSave.Email);
          request.addParameter('EmployeeNumber', tedious.Types.NVarChar, userObjectToSave.EmployeeNumber);
          request.addParameter('EmploymentTypeID', tedious.Types.NVarChar, userObjectToSave.EmploymentTypeID);
          request.addParameter('EnableBreakTime', tedious.Types.Bit, userObjectToSave.EnableBreakTime);
          request.addParameter('EndDate', tedious.Types.NVarChar, userObjectToSave.EndDate);
          request.addParameter('ExpenseApproverID', tedious.Types.NVarChar, userObjectToSave.ExpenseApproverID);
          request.addParameter('GDPRConsentStatus', tedious.Types.NVarChar, userObjectToSave.GDPRConsentStatus);
          request.addParameter('IsActive', tedious.Types.Bit, userObjectToSave.IsActive);
          request.addParameter('MinimumTimeHours', tedious.Types.Float, userObjectToSave.MinimumTimeHours);
          request.addParameter('MinimumTimePeriod', tedious.Types.NVarChar, userObjectToSave.MinimumTimePeriod);
          request.addParameter('Name', tedious.Types.NVarChar, userObjectToSave.Name);
          request.addParameter('Notes', tedious.Types.NVarChar, userObjectToSave.Notes);
          request.addParameter('PayrollType', tedious.Types.NVarChar, userObjectToSave.PayrollType);
          request.addParameter('PreferredTimeEntryView', tedious.Types.NVarChar, userObjectToSave.PreferredTimeEntryView);
          request.addParameter('PreferredTimeFormat', tedious.Types.NVarChar, userObjectToSave.PreferredTimeFormat);
          request.addParameter('RequireComments', tedious.Types.Bit, userObjectToSave.RequireComments);
          request.addParameter('RequireStartEndTime', tedious.Types.Bit, userObjectToSave.RequireStartEndTime);
          request.addParameter('RequireStopwatch', tedious.Types.Bit, userObjectToSave.RequireStopwatch);
          request.addParameter('Role', tedious.Types.NVarChar, userObjectToSave.Role);
          request.addParameter('SecurityLevel', tedious.Types.NVarChar, userObjectToSave.SecurityLevel);
          request.addParameter('SkipWeekend', tedious.Types.Bit, userObjectToSave.SkipWeekend);
          request.addParameter('StartDate', tedious.Types.NVarChar, userObjectToSave.StartDate);
          request.addParameter('SubjectToExpenseApproval', tedious.Types.Bit, userObjectToSave.SubjectToExpenseApproval);
          request.addParameter('SubjectToTimesheetApproval', tedious.Types.Bit, userObjectToSave.SubjectToTimesheetApproval);
          request.addParameter('SubjectToTimesheetCompletion', tedious.Types.Bit, userObjectToSave.SubjectToTimesheetCompletion);
          request.addParameter('TimeOffApproverID', tedious.Types.NVarChar, userObjectToSave.TimeOffApproverID);
          request.addParameter('TimesheetApproverID', tedious.Types.NVarChar, userObjectToSave.TimesheetApproverID);
          request.addParameter('UseCompanyBillingRate', tedious.Types.Bit, userObjectToSave.UseCompanyBillingRate);
          request.addParameter('CreatedSuccessfully', tedious.Types.Bit, createdSuccessFully);
          request.addParameter('ImportJobComments', tedious.Types.NVarChar, importJobComments);
          request.on('row', function (columns) {
            columns.forEach(function (column) {
              if (column.value === null) {
                console.log('NULL');
              } else {
                insertedRecordID = column.value;
                console.log('Record ID of inserted item is ' + column.value);
              }
            });
          });
          request.on('requestCompleted', async function () {
            // release the connection back to the pool when finished
            connection.release();
            if(insertedRecordID !== 0 && userObjectToSave.hasOwnProperty('CustomFields') && userObjectToSave.CustomFields) {
              const customFieldNames = Object.keys(userObjectToSave.CustomFields);
              for (const customFieldName of customFieldNames) {
                var customFieldValue = JSON.stringify(userObjectToSave.CustomFields[customFieldName]);
                await generalOperations.insertCustomFieldsData({
                  recordID: insertedRecordID,
                  entityTypeID: entityTypeID,
                  customFieldName: customFieldName,
                  customFieldValue: customFieldValue,
                  connectionPool: connectionPool
                }).catch(error => {
                  console.log(error);
                });
              }
            }
            resolve();
          });
          connection.execSql(request);
        }
      });
    } catch(ex) {
      console.error(ex.message);
      reject(ex.message);
    }
  });
};
