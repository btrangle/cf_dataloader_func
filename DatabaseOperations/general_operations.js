// import Entity Types constant from app_constants module
const tedious = require('./tedious_implementation');
// import Entity Types constant from app_constants module
const entityTypes = require('../app_constants').EntityType;
// import TableNames constant from app_constants module
const tableNames = require('../app_constants').TableNames;
// import tryParseJSON method from general utilities module to validate JSON
const tryParseJSON = require('../general_utilities').tryParseJSON;
/**
 * This Module provide the methods to perform database opertaiuons such as insertion,updation,slection
 */
module.exports = {
  // pool : pool,
  /**
    * Get BatchRequest Body for Executing Get Requests using Batch Endpoint of ClickTime API
    * @param integer entityTypeID - entityTypeID Constant value
    * @param string userID - UserID extracted from ME response using User Authentication Token
    * @param string companyID - CompanyID extracted from Company endpoint using User Authentication Token
    * @param integer totalRecords - Total Records to Import
    * @param integer importJobStatusID - Job Status ID from ImportJobStatus constant
    */
  createImportJob: async function ({ entityTypeID, userID, userEmail, userName, companyID, totalRecords, importJobStatusID, authToken }) {
    return new Promise((resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        let insertedJobID = 0;
        // acquire a connection from, pool if it's not null
        if(connectionPool != null) {
          connectionPool.acquire(function (err, connection) {
            if (err) {
              console.error(err.message);
              reject(err.message);
            } else {
              const request = new tedious.Request('INSERT INTO [dbo].[DataImportJob]([EntityTypeID],[UserID],[UserEmail],[UserName],[CompanyID],[TotalRecords],[ImportJobStatusID],[CreatedDate],[AuthToken]) OUTPUT INSERTED.ImportJobID VALUES(@EntityTypeID,@UserID,@UserEmail,@UserName,@CompanyID,@TotalRecords,@ImportJobStatusID,CURRENT_TIMESTAMP,@AuthToken);', function (err, rowCount) {
                if (err) {
                  console.error(err.message);
                  connection.release();
                  reject(err.message);
                }
              });
              request.addParameter('EntityTypeID', tedious.Types.Int, entityTypeID);
              request.addParameter('UserID', tedious.Types.NVarChar, userID);
              request.addParameter('UserEmail', tedious.Types.NVarChar, userEmail);
              request.addParameter('UserName', tedious.Types.NVarChar, userName);
              request.addParameter('CompanyID', tedious.Types.NVarChar, companyID);
              request.addParameter('TotalRecords', tedious.Types.Int, totalRecords);
              request.addParameter('ImportJobStatusID', tedious.Types.Int, importJobStatusID);
              request.addParameter('AuthToken', tedious.Types.NVarChar, authToken);
              request.on('row', function (columns) {
                columns.forEach(function (column) {
                  if (column.value === null) {
                    console.log('NULL');
                    // release the connection back to the pool when finished
                    connection.release();
                    resolve(insertedJobID);
                  } else {
                    insertedJobID = column.value;
                    console.log('Job ID of inserted item is ' + column.value);
                    resolve(insertedJobID);
                  }
                });
              });
              request.on('requestCompleted', async function () {
                // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
              });
              connection.execSql(request);
            }
          });
        }
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  },
  /**
  * Get EntityTypeID from Database for Specified Import Job
  */
  getEntityTypeIDByImportJobID: async function (importJobID) {
    return new Promise((resolve, reject) => {
    // get connection pool
      const connectionPool = tedious.getConnectionPool();
      let entityTypeID = 0;
      try{
      // acquire a connection from pool
        if(connectionPool != null) {
          connectionPool.acquire(function (err, connection) {
            if (err) {
              console.error(err.message);
              reject(err.message);
            } else {
              const getEntityTypeIDByImportJobIDRequest = new tedious.Request('SELECT EntityTypeID FROM [dbo].[DataImportJob] WHERE [ImportJobID]=@ImportJobID;', function (err, rowCount) {
                if (err) {
                // release the connection back to the pool when finished
                  connection.release();
                  console.error(err.message);
                  return reject(err.message);
                }
              });
              getEntityTypeIDByImportJobIDRequest.addParameter('ImportJobID', tedious.Types.Int, importJobID);
              connection.execSql(getEntityTypeIDByImportJobIDRequest);

              getEntityTypeIDByImportJobIDRequest.on('row', function (columns) {
                columns.forEach(function (column) {
                  entityTypeID = column.value;
                });
              });
              getEntityTypeIDByImportJobIDRequest.on('requestCompleted', async function () {
              // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
                resolve(entityTypeID);
              });
            }
          });
        } else{
          reject();
        }
      } catch(ex) {
        console.error(ex.message);
        tedious.drainPool(connectionPool);
        reject(ex.message);
      }
    });
  },
  /**
  * Update import job status
  * @param integer importJobID - ImportJob Identifier from Database
  * @param integer importJobStatusID - User Object
  */
  updateImportJobStatus: async function ({ importJobID, importJobStatusID, comments, startTime, endTime }) {
    return new Promise((resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        // acquire a connection from pool
        if(connectionPool != null) {
          connectionPool.acquire(function (err, connection) {
            if (err) {
              console.error(err.message);
              reject(err.message);
            } else {
              var queryToUpdate = 'UPDATE [dbo].[DataImportJob] SET [ImportJobStatusID]=@ImportJobStatusID';
              if(comments && comments.length > 0) {
                queryToUpdate += ',Comments=@Comments';
              }
              if(startTime) {
                queryToUpdate += ',StartTime=@StartTime';
              }
              if(endTime) {
                queryToUpdate += ',EndTime=@EndTime,AuthToken=Null';
              }
              queryToUpdate += ' WHERE ImportJobID=@ImportJobID;';
              const updateImportJobStatusRequest = new tedious.Request(queryToUpdate, function (err, rowCount) {
                if (err) {
                  // release the connection back to the pool when finished
                  connection.release();
                  console.error(err.message);
                  reject(err.message);
                }
              });
              updateImportJobStatusRequest.addParameter('ImportJobStatusID', tedious.Types.Int, importJobStatusID);
              updateImportJobStatusRequest.addParameter('ImportJobID', tedious.Types.Int, importJobID);
              if(comments && comments.length > 0) {
                updateImportJobStatusRequest.addParameter('Comments', tedious.Types.NVarChar, comments);
              }
              if(startTime) {
                updateImportJobStatusRequest.addParameter('StartTime', tedious.Types.DateTime, startTime);
              }
              if(endTime) {
                updateImportJobStatusRequest.addParameter('EndTime', tedious.Types.DateTime, endTime);
              }
              connection.execSql(updateImportJobStatusRequest);
              updateImportJobStatusRequest.on('requestCompleted', async function () {
                // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
                resolve(true);
              });
            }
          });
        } else{
          reject('Unable to qcquire connection from connection pool.');
        }
      } catch(ex) {
        console.error(ex.message);
        tedious.drainPool(connectionPool);
        reject(ex.message);
      }
    });
  },
  /**
    * Save User's Data into ImportJob database for tracking progress of job
    * @param integer createRecordIDFromAPI - Created Record ID from API in case of Successful Request
    * @param string recordID - RecordID of processed record in SQL Database
    * @param string apiResponseCode - API Response Status code along with errors JSON incase
    * @param string apiDetailResponse - API Response Object or errors array in JSON incase of error
    * @param boolean isSuccessfullyCreated - Boolean value to indicate, Either user Was created or not
    */
  updateRecordStatus: async function ({ createRecordIDFromAPI, recordID, apiResponseCode, apiDetailResponse, isSuccessfullyCreated, entityTypeID }) {
    return new Promise((resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try {
        // acquire a connection from pool
        connectionPool.acquire(function (err, connection) {
          if (err) {
            console.error(err.message);
            reject(err.message);
          } else {
            const queryToUpdate = getSQLQueryByEntityTypeIDToUpdateRecord(entityTypeID);
            const updateUserDataRecordStatusRequest = new tedious.Request(queryToUpdate, function (err, rowCount) {
              if (err) {
                // release the connection back to the pool when finished
                connection.release();
                console.error(err.message);
                reject(err.message);
              }
            });
            updateUserDataRecordStatusRequest.addParameter('RecordID', tedious.Types.Int, recordID);
            if(entityTypeID !== entityTypes.Allocations) {
              updateUserDataRecordStatusRequest.addParameter('ID', tedious.Types.NVarChar, createRecordIDFromAPI);
            }
            updateUserDataRecordStatusRequest.addParameter('CreatedSuccessfully', tedious.Types.Bit, isSuccessfullyCreated);
            updateUserDataRecordStatusRequest.addParameter('APIResponseCode', tedious.Types.NVarChar, apiResponseCode);
            updateUserDataRecordStatusRequest.addParameter('APIDetailResponse', tedious.Types.NVarChar, apiDetailResponse);
            connection.execSql(updateUserDataRecordStatusRequest);
            updateUserDataRecordStatusRequest.on('requestCompleted', async function () {
              // release the connection back to the pool when finished
              connection.release();
              tedious.drainPool(connectionPool);
              resolve();
            });
          }
        });
      } catch(ex) {
        console.error(ex.message);
        tedious.drainPool(connectionPool);
        reject(ex.message);
      }
    });
  },
  /**
  * Get InProcess Job Count
  */
  getInProcessJobCount: async function () {
    return new Promise((resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        var jobCount = 0;
        // acquire a connection from pool
        if(connectionPool != null) {
          connectionPool.acquire(function (err, connection) {
            if (err) {
              console.error(err.message);
              reject(err.message);
            } else {
              const queryToGetJobCount = 'SELECT COUNT(distinct([UserID])) AS TotalCount from [dbo].[DataImportJob] where [ImportJobStatusID] = 3;';
              const getJobCountByStatusIDRequest = new tedious.Request(queryToGetJobCount, function (err, rowCount) {
                if (err) {
                  // release the connection back to the pool when finished
                  connection.release();
                  console.error(err.message);
                  reject(err.message);
                }
              });
              connection.execSql(getJobCountByStatusIDRequest);
              getJobCountByStatusIDRequest.on('row', function (columns) {
                columns.forEach(function (column) {
                  if (column.value === null) {
                    console.log('NULL');
                  } else {
                    jobCount = column.value;
                    console.log(`In Process jobCount: ${jobCount}`);
                  }
                });
              });
              getJobCountByStatusIDRequest.on('requestCompleted', async function () {
                // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
                resolve(jobCount);
              });
            }
          });
        }
      } catch(ex) {
        console.error(ex.message);
        tedious.drainPool(connectionPool);
        reject(ex.message);
      }
    });
  },
  /**
  * Get All Jobs with status "Not Started"  for a user who has oldest job with "Not Started" status in the database
  */
  getOldestImportJobs: async function () {
    return new Promise((resolve, reject) => {
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      var notStartedJobs = [];
      try{
        // acquire a connection from pool
        if(connectionPool != null) {
          connectionPool.acquire(function (err, connection) {
            if (err) {
              console.error(err.message);
              reject(err.message);
            } else {
              const getAllJobsOfUserWithOldestJobIDRequest = new tedious.Request('SELECT * FROM [dbo].[DataImportJob] where [ImportJobStatusID]=1 AND[UserID] = (SELECT TOP 1(USERID) FROM [dbo].[DataImportJob] where [ImportJobStatusID]=1 ORDER BY [ImportJobID]);', function (err, rowCount) {
                if (err) {
                  // release the connection back to the pool when finished
                  connection.release();
                  console.error(err.message);
                  reject(err.message);
                }
              });
              connection.execSql(getAllJobsOfUserWithOldestJobIDRequest);

              getAllJobsOfUserWithOldestJobIDRequest.on('row', function (columns) {
                var notStartedJob = {};
                columns.forEach(function (column) {
                  notStartedJob[column.metadata.colName] = column.value;
                });
                notStartedJobs.push(notStartedJob);
              });
              getAllJobsOfUserWithOldestJobIDRequest.on('requestCompleted', async function () {
                // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
                resolve(notStartedJobs);
              });
            }
          });
        } else{
          reject();
        }
      } catch(ex) {
        console.error(ex.message);
        tedious.drainPool(connectionPool);
        reject(ex.message);
      }
    });
  },
  /**
  * Get All Jobs with status "Not Started"  for a user who has oldest job with "Not Started" status in the database
  */
  getImportJobsListWithStatus: async function ({ userID, importJobStatusID }) {
    return new Promise(async (resolve, reject) => {
      var importJobsListWithStatus = [];
      try{
        for (const entityTypeID of Object.values(entityTypes)) {
          try{
            await getImportJobsList({
              entityTypeID: entityTypeID,
              userID: userID,
              importJobStatusID: importJobStatusID
            }).then(importJobsList => {
              if(importJobsList != null && importJobsList.length > 0) {
                importJobsListWithStatus.push({
                  EntityType: entityTypeID,
                  JobsList: importJobsList
                });
              }
            }).catch(error => {
              console.error(error);
            });
          }catch(ex) {
            console.error(ex);
          }
        }
        resolve(importJobsListWithStatus);
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  },
  /**
  * Get All records which need to be prcoessed under a Job
  */
  getImportJobData: async function ({ importJobID, reAttemptLimit, createdSuccessfully, entityTypeID }) {
    return new Promise(async (resolve, reject) => {
      let importJobData = [];
      try{
        const queryToGetImportJobData = getSQLQueryToGetImportJobData({
          entityTypeID: entityTypeID,
          createdSuccessfully: createdSuccessfully,
          reAttemptLimit: reAttemptLimit
        });
        switch (entityTypeID) {
          case entityTypes.Allocations:
            importJobData = await getDataWithoutCustomFields({
              importJobID: importJobID,
              queryToGetImportJobData: queryToGetImportJobData
            }).catch(error => {
              console.error(error);
            });
            break;
          default:
            // except Allocations Entity, all Entities has custom fields
            importJobData = await getDataWithCustomFields({
              importJobID: importJobID,
              queryToGetImportJobData: queryToGetImportJobData
            }).catch(error => {
              console.error(error);
            });
            break;
        }
        resolve(importJobData);
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  },
  /**
  * Get Import Job Summary
  */
  getImportJobSummary: async function ({ importJobID, entityTypeID }) {
    return new Promise((resolve, reject) => {
      var importJobSummary = {};
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        // acquire a connection from pool
        if(connectionPool != null) {
          connectionPool.acquire(function (err, connection) {
            if (err) {
              console.error(err.message);
              reject(err.message);
            } else {
              const tableName = getEntityTableName(entityTypeID);
              const getImportJobSummaryRequest = new tedious.Request(`SELECT IJ.[ImportJobID],[TotalRecords],
                            SUM((CASE WHEN JD.CreatedSuccessfully = 0 THEN 0 ELSE 1 END)) AS CreatedSuccessfully,
                            SUM((CASE WHEN JD.CreatedSuccessfully = 0 THEN 1 ELSE 0 END)) AS FailedToCreat 
                            from [dbo].[DataImportJob] IJ
                            inner join [${tableName}] JD on IJ.ImportJobID = JD.ImportJobID
                            WHERE JD.[ImportJobID]=@ImportJobID
                            GROUP BY IJ.[ImportJobID],[TotalRecords];`, function (err, rowCount) {
                if (err) {
                  // release the connection back to the pool when finished
                  connection.release();
                  tedious.drainPool(connectionPool);
                  console.error(err.message);
                  reject(err.message);
                }
              });
              getImportJobSummaryRequest.addParameter('ImportJobID', tedious.Types.Int, importJobID);
              connection.execSql(getImportJobSummaryRequest);
              getImportJobSummaryRequest.on('row', function (columns) {
                importJobSummary['EntityType'] = Object.keys(entityTypes).find(key => entityTypes[key] === entityTypeID);
                columns.forEach(function (column) {
                  importJobSummary[column.metadata.colName] = column.value;
                });
              });
              getImportJobSummaryRequest.on('requestCompleted', async function () {
                // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
                resolve(importJobSummary);
              });
            }
          });
        } else{
          reject('Unable to acquire SQL connection');
        }
      } catch(ex) {
        console.error(ex.message);
        tedious.drainPool(connectionPool);
        reject(ex.message);
      }
    });
  },
  /**
  * Get Import Job Summary
  */
  getImportJobDetailSummary: async function ({ importJobID, userID, entityTypeID }) {
    return new Promise((resolve, reject) => {
      var importJobSummary = {};
      // get connection pool
      const connectionPool = tedious.getConnectionPool();
      try{
        // acquire a connection from pool
        connectionPool.acquire(function (err, connection) {
          if (err) {
            console.error(err.message);
            reject(err.message);
          } else {
            const tableName = getEntityTableName(entityTypeID);
            const getImportJobSummaryRequest = new tedious.Request(`SELECT IJ.[ImportJobID] as [Job Number],
                        IJ.[CreatedDate] as [Job Creation],
                        IJ.[StartTime] as [Job Start Time],
                        IJ.[EndTime] as [Job Completion Time],
                        IJ.[TotalRecords] as [Total Records In Request],
                        SUM((CASE WHEN JD.CreatedSuccessfully is not null THEN 1 ELSE 0 END)) AS [Total Records Processed],
                        SUM((CASE WHEN JD.CreatedSuccessfully = 0 THEN 0 ELSE 1 END)) AS [Total Processed - Success],
                        SUM((CASE WHEN JD.CreatedSuccessfully = 0 THEN 1 ELSE 0 END)) AS [Total Processed - Errors] 
                        from [dbo].[DataImportJob] IJ
                        inner join [${tableName}] JD on IJ.ImportJobID = JD.ImportJobID
                        where JD.[ImportJobID]=@ImportJobID and IJ.[UserID]=@UserID
                        GROUP BY IJ.[ImportJobID],[TotalRecords],[CreatedDate],[StartTime],[EndTime];`, function (err, rowCount) {
              if (err) {
                // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
                console.error(err.message);
                reject(err.message);
              }
            });
            getImportJobSummaryRequest.addParameter('ImportJobID', tedious.Types.Int, importJobID);
            getImportJobSummaryRequest.addParameter('UserID', tedious.Types.NVarChar, userID);
            connection.execSql(getImportJobSummaryRequest);
            getImportJobSummaryRequest.on('row', function (columns) {
              importJobSummary['EntityTypeID'] = entityTypeID;
              columns.forEach(function (column) {
                importJobSummary[column.metadata.colName] = column.value;
              });
            });
            getImportJobSummaryRequest.on('requestCompleted', async function () {
              // release the connection back to the pool when finished
              connection.release();
              tedious.drainPool(connectionPool);
              resolve(importJobSummary);
            });
          }
        });
      } catch(ex) {
        console.error(ex.message);
        tedious.drainPool(connectionPool);
        reject(ex.message);
      }
    });
  },
  /**
* Save User's Data into ImportJob database for tracking progress of job
* @param integer recordID - ImportJob Identifier from Database
* @param string customFieldName - User Object
* @param string customFieldValue - User Object
* @param Object connectionPool -Tedious Connection Pool
*/
  insertCustomFieldsData: async function ({ recordID, entityTypeID, customFieldName, customFieldValue, connectionPool }) {
    return new Promise((resolve, reject) => {
      try{
      // acquire a connection from pool
        connectionPool.acquire(function (err, connection) {
          if (err) {
            console.error(err.message);
            reject(err.message);
          } else {
            const customFieldInsertionRequest = new tedious.Request('INSERT INTO [dbo].[CustomFieldsData]([RecordID],[EntityTypeID],[FieldName],[Value]) VALUES(@RecordID,@EntityTypeID,@FieldName,@Value);', function (err, rowCount) {
              if (err) {
              // release the connection back to the pool when finished
                connection.release();
                console.error(err.message);
                reject(err.message);
              }
            });
            customFieldInsertionRequest.addParameter('RecordID', tedious.Types.Int, recordID);
            customFieldInsertionRequest.addParameter('EntityTypeID', tedious.Types.Int, entityTypeID);
            customFieldInsertionRequest.addParameter('FieldName', tedious.Types.NVarChar, customFieldName);
            customFieldInsertionRequest.addParameter('Value', tedious.Types.NVarChar, customFieldValue);
            connection.execSql(customFieldInsertionRequest);
            customFieldInsertionRequest.on('requestCompleted', async function () {
            // release the connection back to the pool when finished
              connection.release();
              resolve();
            });
          }
        });
      } catch(ex) {
        console.error(ex.message);
        reject(ex.message);
      }
    });
  }
};

// Function without export
function getSQLQueryToGetImportJobData ({ entityTypeID, createdSuccessfully, reAttemptLimit }) {
  let queryToGetImportJobData = '';
  switch (entityTypeID) {
    case entityTypes.Allocations:
      queryToGetImportJobData = 'SELECT * FROM [Allocations] jd WHERE jd.[ImportJobID]=@ImportJobID';
      break;
    default:
      const tableNameForEntity = getEntityTableName(entityTypeID);
      queryToGetImportJobData = `SELECT * FROM [dbo].[${tableNameForEntity}] jd
      left join [CustomFieldsData] cf on jd.RecordID = cf.RecordID and cf.EntityTypeID = ${entityTypeID}
      WHERE jd.[ImportJobID]=@ImportJobID`;
      break;
  }

  if(createdSuccessfully === true) {
    queryToGetImportJobData += ' and [CreatedSuccessfully]=1';
  } else if(createdSuccessfully === false) {
    queryToGetImportJobData += ' and [CreatedSuccessfully]=0';
  } else if(reAttemptLimit) {
    queryToGetImportJobData += ` and (CreatedSuccessfully is null or (APIResponseCode = 429 and TotalAttempts < ${reAttemptLimit}))`;
  }
  return queryToGetImportJobData;
}

function getEntityTableName (entityTypeID) {
  let tableName = '';
  switch (entityTypeID) {
    case entityTypes.Users:
      tableName = tableNames.Users;
      break;
    case entityTypes.Allocations:
      tableName = tableNames.Allocations;
      break;
    case entityTypes.Jobs:
      tableName = tableNames.Jobs;
      break;
    case entityTypes.Tasks:
      tableName = tableNames.Tasks;
      break;
    case entityTypes.TimeEntries:
      tableName = tableNames.TimeEntries;
      break;
    case entityTypes.Divisions:
      tableName = tableNames.Divisions;
      break;
    case entityTypes.Clients:
      tableName = tableNames.Clients;
      break;
    default:
      console.log('Invalid Entity ID for getting Query');
      tableName = 'Invalid Entity ID';
  }
  return tableName;
}

function getSQLQueryByEntityTypeIDToUpdateRecord (entityTypeID) {
  let tableSpecificQueryPart = '';
  const entityTableName = getEntityTableName(entityTypeID);
  switch (entityTypeID) {
    case entityTypes.Allocations:
      tableSpecificQueryPart = ` ${entityTableName} SET `;
      break;
    default:
      tableSpecificQueryPart = ` ${entityTableName} SET [ID]=@ID,`;
      break;
  }
  const queryToUpdate = `UPDATE ${tableSpecificQueryPart} [CreatedSuccessfully]=@CreatedSuccessfully,[APIResponseCode]=@APIResponseCode,[APIDetailResponse]=@APIDetailResponse,[TotalAttempts]=[TotalAttempts]+1 WHERE [RecordID]=@RecordID`;
  return queryToUpdate;
}

async function getDataWithCustomFields ({ importJobID, queryToGetImportJobData }) {
  return new Promise((resolve, reject) => {
    var importJobData = [];
    // get connection pool
    const connectionPool = tedious.getConnectionPool();
    try{
      // acquire a connection from pool
      if(connectionPool != null) {
        connectionPool.acquire(function (err, connection) {
          if (err) {
            console.error(err.message);
            connection.release();
            tedious.drainPool(connectionPool);
            reject(err.message);
          } else {
            const getImportJobDataRequest = new tedious.Request(queryToGetImportJobData, function (err, rowCount) {
              if (err) {
                // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
                console.error(err.message);
                reject(err.message);
              }
            });
            getImportJobDataRequest.addParameter('ImportJobID', tedious.Types.Int, importJobID);
            connection.execSql(getImportJobDataRequest);

            var importJobRecord = {};
            var customFields = {};
            var lastRecordID = 0;
            getImportJobDataRequest.on('row', function (columns) {
              var fieldName = '';
              if(lastRecordID !== columns[0].value) {
                if(lastRecordID !== 0) {
                  importJobRecord['CustomFields'] = customFields;
                  importJobData.push(importJobRecord);
                  importJobRecord = {};
                  customFields = {};
                }
                columns.forEach(function (column) {
                  switch(column.metadata.colName) {
                    case 'FieldName':
                      fieldName = column.value;
                      break;
                    case 'Value':
                      const customFieldValue = tryParseJSON(column.value);
                      if(customFieldValue !== false) {
                        customFields[fieldName] = customFieldValue;
                      }
                      break;
                    case 'CustomFieldID':
                      break;
                    case 'RecordID':
                      if(column.value != null) {
                        importJobRecord[column.metadata.colName] = column.value;
                      }
                      break;
                    default:
                      if(column.value && column.value != null && column.value !== '') {
                        importJobRecord[column.metadata.colName] = column.value;
                      }
                  }
                });
              } else {
                const stratingIndexOfCustomFields = columns.findIndex(c => c.metadata.colName === 'CustomFieldID');
                if(stratingIndexOfCustomFields > 0) {
                  const fieldNameColumnIndex = stratingIndexOfCustomFields + 3;
                  const fieldValueColumnIndex = stratingIndexOfCustomFields + 4;
                  const customFieldValue = tryParseJSON(columns[fieldValueColumnIndex].value);
                  if(customFieldValue !== false) {
                    customFields[columns[fieldNameColumnIndex].value] = customFieldValue;
                  }
                }
              }
              lastRecordID = columns[0].value;
            });
            getImportJobDataRequest.on('requestCompleted', async function () {
              // release the connection back to the pool when finished
              importJobRecord['CustomFields'] = customFields;
              importJobData.push(importJobRecord);
              connection.release();
              tedious.drainPool(connectionPool);
              resolve(importJobData);
            });
          }
        });
      } else{
        reject();
      }
    } catch(ex) {
      console.error(ex.message);
      reject(ex.message);
    }
  });
}

async function getDataWithoutCustomFields ({ importJobID, queryToGetImportJobData }) {
  return new Promise((resolve, reject) => {
    var importJobData = [];
    // get connection pool
    const connectionPool = tedious.getConnectionPool();
    try{
      // acquire a connection from pool
      if(connectionPool != null) {
        connectionPool.acquire(function (err, connection) {
          if (err) {
            console.error(err.message);
            connection.release();
            tedious.drainPool(connectionPool);
            reject(err.message);
          } else {
            const getImportJobDataRequest = new tedious.Request(queryToGetImportJobData, function (err, rowCount) {
              if (err) {
                // release the connection back to the pool when finished
                connection.release();
                tedious.drainPool(connectionPool);
                console.error(err.message);
                reject(err.message);
              }
            });
            getImportJobDataRequest.addParameter('ImportJobID', tedious.Types.Int, importJobID);
            connection.execSql(getImportJobDataRequest);

            getImportJobDataRequest.on('row', function (columns) {
              var importJobRecord = {};
              columns.forEach(function (column) {
                if(column.value && column.value != null && column.value !== '') {
                  importJobRecord[column.metadata.colName] = column.value;
                }
              });
              importJobData.push(importJobRecord);
            });
            getImportJobDataRequest.on('requestCompleted', async function () {
              // release the connection back to the pool when finished
              connection.release();
              tedious.drainPool(connectionPool);
              resolve(importJobData);
            });
          }
        });
      } else{
        reject();
      }
    } catch(ex) {
      console.error(ex.message);
      reject(ex.message);
    }
  });
}

async function getImportJobsList ({ entityTypeID, userID, importJobStatusID }) {
  return new Promise((resolve, reject) => {
    const importJobsListWithStatus = [];
    // get connection pool
    const connectionPool = tedious.getConnectionPool();
    try{
      // acquire a connection from pool
      if(connectionPool != null) {
        connectionPool.acquire(function (err, connection) {
          if (err) {
            console.error(err.message);
            reject(err.message);
          } else {
            const tableName = getEntityTableName(entityTypeID);
            let queryToGetImportJobsWithStatus = `SELECT IJ.[ImportJobID] as [Job Number],
                            JS.ImportJobStatus as [Job Status],
                            IJ.[CreatedDate] as [Job Creation],
                            IJ.[StartTime] as [Job Start Time],
                            IJ.[EndTime] as [Job Completion Time],
                            IJ.[TotalRecords] as [Total Records In Request],
                            SUM((CASE WHEN JD.CreatedSuccessfully is not null THEN 1 ELSE 0 END)) AS [Total Records Processed],
                            SUM((CASE WHEN JD.CreatedSuccessfully = 0 THEN 0 ELSE 1 END)) AS [Total Processed - Success],
                            SUM((CASE WHEN JD.CreatedSuccessfully = 0 THEN 1 ELSE 0 END)) AS [Total Processed - Errors] 
                            from [dbo].[DataImportJob] IJ
                            inner join [${tableName}] JD on IJ.ImportJobID = JD.ImportJobID
                            inner join [ImportJobStatus] JS on JS.ImportJobStatusID = IJ.ImportJobStatusID
                            where IJ.UserID=@UserID`;
            if(importJobStatusID != null) {
              queryToGetImportJobsWithStatus += ' and IJ.ImportJobStatusID=@ImportJobStatusID';
            }
            queryToGetImportJobsWithStatus += ' GROUP BY IJ.[ImportJobID],[TotalRecords],[CreatedDate],[StartTime],[EndTime],JS.[ImportJobStatus]';
            const getImportJobsListWithStatusRequest = new tedious.Request(queryToGetImportJobsWithStatus, function (err, rowCount) {
              if (err) {
                // release the connection back to the pool when finished
                connection.release();
                console.error(err.message);
              }
            });
            getImportJobsListWithStatusRequest.addParameter('UserID', tedious.Types.NVarChar, userID);
            if(importJobStatusID != null) {
              getImportJobsListWithStatusRequest.addParameter('ImportJobStatusID', tedious.Types.NVarChar, importJobStatusID);
            }
            connection.execSql(getImportJobsListWithStatusRequest);
            getImportJobsListWithStatusRequest.on('row', function (columns) {
              var importJobWithStatus = {};
              columns.forEach(function (column) {
                importJobWithStatus[column.metadata.colName] = column.value;
              });
              importJobsListWithStatus.push(importJobWithStatus);
            });
            getImportJobsListWithStatusRequest.on('requestCompleted', async function () {
              // release the connection back to the pool when finished
              connection.release();
              tedious.drainPool(connectionPool);
              resolve(importJobsListWithStatus);
            });
          }
        });
      } else{
        reject('Unable to acquire SQL connection');
      }
    } catch(ex) {
      console.error(ex.message);
      tedious.drainPool(connectionPool);
      reject(ex.message);
    }
  });
};
