// Import getResponseObject method from general utilities module
const getResponseObject = require('../general_utilities').getResponseObject;
const entityTypes = require('../app_constants').EntityType;
module.exports = async function (context, req) {
  context.log('Request Recieved for Import.');
  // Validate query parameters
  const requestValidation = await validateRequest(req);
  console.log(requestValidation);
  if (requestValidation.ValidationStatus) {
    const totalRecords = req.body.data.length;
    // import encrypt and decrypt module for node.js
    const Cryptr = require('cryptr');
    const cryptr = new Cryptr(process.env['EncryptionKey']);
    const encryptedAuthToken = cryptr.encrypt(req.body.Token);
    // Import Database Operations Module
    var databaseOperations = require('../DatabaseOperations/general_operations');
    // create import job in database
    const createdJObID = await databaseOperations.createImportJob({
      entityTypeID: req.body.EntityType,
      userID: requestValidation.UserID,
      userEmail: requestValidation.UserEmail,
      userName: requestValidation.UserName,
      companyID: requestValidation.CompanyID,
      totalRecords: totalRecords,
      // Import ImportJobStatus constats from App Constants
      importJobStatusID: require('../app_constants').ImportJobStatus.NotStarted,
      authToken: encryptedAuthToken
    });

    // Save data into database according to Entity Type
    switch (req.body.EntityType) {
      case entityTypes.Users:
        // save users data into database
        await require('../DatabaseOperations/users').saveUsersDataToDatabase({
          importJobID: createdJObID,
          usersData: req.body.data,
          entityTypeID: req.body.EntityType
        });
        break;
      case entityTypes.Allocations:
        // save allocations data into database
        await require('../DatabaseOperations/time_allocations').saveAllocationsDataToDatabase({
          importJobID: createdJObID,
          allocationsData: req.body.data
        });
        break;
      case entityTypes.Jobs:
        // save Jobs data into database
        await require('../DatabaseOperations/jobs').saveJobsDataToDatabase({
          importJobID: createdJObID,
          jobsData: req.body.data,
          entityTypeID: req.body.EntityType
        });
        break;
      case entityTypes.Tasks:
        // save Taks data into database
        await require('../DatabaseOperations/tasks').saveTaksDataToDatabase({
          importJobID: createdJObID,
          tasksData: req.body.data,
          entityTypeID: req.body.EntityType
        });
        break;
      case entityTypes.TimeEntries:
        // save Taks data into database
        await require('../DatabaseOperations/time_entries').saveTimeEntriesDataToDatabase({
          importJobID: createdJObID,
          timeEntriesData: req.body.data,
          entityTypeID: req.body.EntityType
        });
        break;
      case entityTypes.Divisions:
        // save Divisions data into database
        await require('../DatabaseOperations/divisions').saveDivisionsDataToDatabase({
          importJobID: createdJObID,
          divisionsData: req.body.data,
          entityTypeID: req.body.EntityType
        });
        break;
      case entityTypes.Clients:
        // save Divisions data into database
        await require('../DatabaseOperations/clients').saveClientsDataToDatabase({
          importJobID: createdJObID,
          clientsData: req.body.data,
          entityTypeID: req.body.EntityType
        });
        break;
      default:
        break;
    }
    // concatinate ClikTime UserID and CompanyID to genrate QueueName for a user
    const queueName = `${requestValidation.UserID.toLowerCase()}00${requestValidation.CompanyID.toLowerCase()}`;
    // Call function to manage Azure Storage Queue method by importing it from azure_storage_queue_manager module
    require('../azure_storage_queue_manager').manageAzureStorageQueue({
      queueName: queueName,
      importJobID: createdJObID,
      entityTypeID: req.body.EntityType,
      userEmail: requestValidation.UserEmail,
      userName: requestValidation.UserName,
      authToken: encryptedAuthToken
    });
    context.res = getResponseObject({
      status: 201,
      body: { JobNumber: createdJObID }
    });
    context.done();
  } else {
    // Response back if request is not valid
    context.res = getResponseObject({
      status: requestValidation.ResponseCode,
      body: requestValidation.ResponseMessage
    });
    context.done();
  }
};

/**
 * Validate Http Request Parameters
 * @param HttpRequestObject HttpRequest Object
 * @return boolean true or false
 * IF request is valid return true
 */
async function validateRequest (req) {
  return new Promise(async (resolve, reject) => {
    const requestValidation = { ValidationStatus: false, ResponseMessage: 'Invalid Request', ResponseCode: 400, UserID: null, UserEmail: null, UserName: null, CompanyID: null };
    try{
      if (req.body && req.body.Token && req.body.data && req.body.EntityType) {
        if(!Object.values(entityTypes).includes(req.body.EntityType)) {
          requestValidation.ResponseMessage = 'Invalid Entity Type';
          return resolve(requestValidation);
        }
        // If All required parameters are sypplied than validate the supplied Token
        // Import Module to Consume ClickTimeAPI
        const userData = await require('../clicktime_api').getUserInfoByAuthToken(req.body.Token);
        if(userData) {
          requestValidation.ValidationStatus = true;
          requestValidation.ResponseMessage = 'Valid Request';
          requestValidation.ResponseCode = 200;
          requestValidation.UserID = userData.ID;
          requestValidation.UserEmail = userData.Email;
          requestValidation.UserName = userData.Name;
          const companyData = await require('../clicktime_api').getUserCompanyInfo(req.body.Token);
          if(companyData) {
            requestValidation.CompanyID = companyData.ID;
          }
        } else{
          requestValidation.ValidationStatus = false;
          requestValidation.ResponseMessage = 'Invalid Authentication Token';
          requestValidation.ResponseCode = 401;
        }
      } else{
        requestValidation.ValidationStatus = false;
        requestValidation.ResponseMessage = 'Please specify values for all parameters';
      }
      resolve(requestValidation);
    } catch(ex) {
      reject(requestValidation);
    }
  });
}
