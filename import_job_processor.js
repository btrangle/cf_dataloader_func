// Import ImportJobStatus constats from App Constants
const ImportJobStatus = require('./app_constants').ImportJobStatus;
// Import Module to Consume ClickTimeAPI
const ClickTimeAPIConsumer = require('./clicktime_api');

// Import Module to Send Emails
const EmailManager = require('./email_manager');

// Import Database Operations Module
const databaseGeneralOperations = require('./DatabaseOperations/general_operations');
// import encrypt and decrypt module for node.js
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env['EncryptionKey']);
const entityTypes = require('./app_constants').EntityType;
module.exports = {
  /**
     *
     * @param {string} queueName
     */
  processImportJob: async function (queueName) {
    // Import Module to Push requests to Azure Storage Queue
    var AzureStorageQueueManager = require('./azure_storage_queue_manager');
    let queueLength = -1;
    do{
      const reAttemptLimit = process.env['MaximumAttemptsLimit'];
      // Get Messages from Azure Storage Queue. Each message in queue refrence to a Import job which need to be processed
      await AzureStorageQueueManager.getMessagesFromQueue({
        queueName: queueName,
        queueLength: queueLength
      }).then(async queueMessages => {
        // Messages retrieved, Itterate through all messages
        for (const queueMessage of queueMessages) {
          // extract the message text and parse it
          const messageObject = JSON.parse(queueMessage.messageText);
          const userEmail = messageObject.UserEmail;
          const userName = messageObject.UserName;
          const entityTypeID = messageObject.EntityTypeID;
          // decrypt the Authentication Token
          const decryptedAuthToken = cryptr.decrypt(messageObject.AuthToken);
          // Check if it's a re attempt due to 429 error, If it's re attempt we don't need to update start time as it's already updated
          if(!messageObject.IsReAttempt) {
            await databaseGeneralOperations.updateImportJobStatus({
              importJobID: messageObject.ImportJobID,
              importJobStatusID: ImportJobStatus.InProcess,
              startTime: getCurrentDateTimeString()
            }).catch(error => {
              console.log(`Exception occured while trying to update job Status: ${error}`);
            });
          } else{
            // If it's re attempt only update the job status to 'In Process' as it was sent back to Pending state due to 429 error
            await databaseGeneralOperations.updateImportJobStatus({
              importJobID: messageObject.ImportJobID,
              importJobStatusID: ImportJobStatus.InProcess
            }).catch(error => {
              console.log(`Exception occured while trying to update job Status: ${error}`);
            });
          }
          // Get Import Job Data from Database
          await databaseGeneralOperations.getImportJobData({
            importJobID: messageObject.ImportJobID,
            reAttemptLimit: reAttemptLimit,
            entityTypeID: entityTypeID
          }).then(async importJobData => {
            // Generate batch requests for a JOB
            if(importJobData && importJobData.length > 0) {
              await ClickTimeAPIConsumer.getBatchRequests({
                importJobData: importJobData,
                endpointURL: getEndpointUrlByEntityTypeID(entityTypeID)
              }).then(async batchRequests => {
                let isAPIThrotalErrorOccuredForJob = false;
                let totalRecordsRejectedWithThrotalErrors = 0;
                for (const batchRequest of batchRequests) {
                  // Itterate through created btach requests and execute all of them for processing a job
                  await ClickTimeAPIConsumer.executeBatchRequest({
                    batchRequest: batchRequest,
                    authToken: decryptedAuthToken,
                    reAttemptLimit: reAttemptLimit,
                    entityTypeID: entityTypeID
                  }).then(async batchRequestResponse => {
                    // Set flag and counter for requests rejects with 429 error
                    if(batchRequestResponse.APIThrotalErrorOccuredForJob) {
                      isAPIThrotalErrorOccuredForJob = true;
                      totalRecordsRejectedWithThrotalErrors = totalRecordsRejectedWithThrotalErrors + batchRequestResponse.RecordsCountForReProcessing;
                      const waitInSecondsAfterThrotalError = Math.floor((process.env['WaitInSecondsAfterThrotalError'] * 1000) % 3600);
                      await sleep(waitInSecondsAfterThrotalError);
                    }
                  }).catch(error => {
                    console.log('Exception occured during execution of a batch request for Job ID' + messageObject.ImportJobID + ': ' + error);
                  });
                }
                // If no API throtal(429) error occured than delete message from queue, update import jon status, send email
                if(!isAPIThrotalErrorOccuredForJob) {
                  // Delete Job ID from Azure Storage Queue created for user
                  await AzureStorageQueueManager.deleteMessageFromQueue({
                    queueName: queueName,
                    message: queueMessage
                  }).then(async messageDeletionStatus => {
                    const jobCompletionComments = messageDeletionStatus.Status ? 'Job Completed Successfully.' : 'Unable to delete from Azure Storage Queue.' + messageDeletionStatus.ErrorMessage;
                    // Update job status to completed
                    await databaseGeneralOperations.updateImportJobStatus({
                      importJobID: messageObject.ImportJobID,
                      importJobStatusID: ImportJobStatus.Completed,
                      comments: jobCompletionComments,
                      endTime: getCurrentDateTimeString()
                    }).catch(error => {
                      console.log(`Exception occured while trying to set job status as completed for Job ID: ${messageObject.ImportJobID}`);
                      console.log(error);
                    });
                    // Get job sumarry for job completion email body
                    await databaseGeneralOperations.getImportJobSummary({
                      importJobID: messageObject.ImportJobID,
                      entityTypeID: entityTypeID
                    }).then(async importJobSummary => {
                      // Send job completion email
                      await EmailManager.sendJobCompletionEmail({
                        recipientName: userName,
                        recipientEmail: userEmail,
                        importJobSummary: importJobSummary
                      }).catch(error => {
                        console.log(`Exception occured while trying to send job completion email for Job ID: ${messageObject.ImportJobID}`);
                        console.log(error);
                      });
                    }).catch(error => {
                      console.log(`Exception occured while trying to get Job Summary to send job completion email for Job ID: ${messageObject.ImportJobID}`);
                      console.log(error);
                    });
                  }).catch(error => {
                    console.log(`Exception occured during execution of a batch request for Job ID: ${messageObject.ImportJobID}`);
                    console.log(error);
                  });
                } else{
                  if(totalRecordsRejectedWithThrotalErrors > 0) {
                    messageObject.IsReAttempt = true;
                    // Update Message in Azure Queue to handle requests rejected with API throtal error(429)
                    await AzureStorageQueueManager.updateMessageInQueue({
                      queueName: queueName,
                      message: queueMessage,
                      messageText: messageObject
                    }).catch(error => {
                      console.log(`Exception occured while trying to update azure queue to handle records with 429 errors for Job ID: ${messageObject.ImportJobID}`);
                      console.error(error);
                    });
                    // Set import job sttaus back to 'Pending' from 'In Process'
                    await databaseGeneralOperations.updateImportJobStatus({
                      importJobID: messageObject.ImportJobID,
                      importJobStatusID: ImportJobStatus.PendingProcess,
                      comments: 'Sent back to pending process state to process records rejected with 429 error code.'
                    }).catch(error => {
                      console.log(`Exception occured while trying to set job status as Pending due to 429 errors for Job ID: ${messageObject.ImportJobID}`);
                      console.error(error);
                    });
                  } else{
                    await databaseGeneralOperations.updateImportJobStatus(messageObject.ImportJobID, ImportJobStatus.Completed, '', null, getCurrentDateTimeString()).catch(error => {
                      console.log(`Exception occured while trying to update job Status for Job ID: ${messageObject.ImportJobID}`);
                      console.error(error);
                    });
                    await databaseGeneralOperations.getImportJobSummary({
                      importJobID: messageObject.ImportJobID,
                      entityTypeID: entityTypeID
                    }).then(async importJobSummary => {
                      await EmailManager.sendJobCompletionEmail({
                        recipientName: userName,
                        recipientEmail: userEmail,
                        importJobSummary: importJobSummary
                      }).catch(error => {
                        console.log(`Exception occured while trying to send job completion email for Job ID: ${messageObject.ImportJobID}`);
                        console.error(error);
                      });
                    }).catch(error => {
                      console.log(`Exception occured while trying to get Job Summary to send job completion email for Job ID: ${messageObject.ImportJobID}`);
                      console.error(error);
                    });
                  }
                }
              }).catch(error => {
                console.log(`Exception occured while trying to get batch requests for Import JOb ID: ${messageObject.ImportJobID}`);
                console.error(error);
              });
            }
          }).catch(error => {
            console.log(`Exception occured while trying to get job data from database for Job ID: ${messageObject.ImportJobID}`);
            console.error(error);
          });
        }
        queueLength = await AzureStorageQueueManager.getQueueLength(queueName).catch(error => {
          console.log(`Exception occured while trying to get Queue Lenghth for Azure Storage Queue: ${queueName}`);
          console.error(error);
        });
      }).catch(error => {
        console.log(`Exception occured while getting messages from Azure Queue: ${queueName}`);
        console.error(error);
      });
    }
    while (queueLength > 0);
    await AzureStorageQueueManager.deleteQueue(queueName).then(queueDeletionStatus => {
      console.log(`Queue ${queueName} Deleted Successfully.`);
    }).catch(error => {
      console.log('Exception occured while trying to delete Azure Queue: ' + queueName);
      console.log(error);
    });
    AzureStorageQueueManager.manageAzureStorageQueue({
      queueName: ''
    }).catch(error => {
      console.log('Exception occured while trying to call \'manageAzureStorageQueue\' function after completion of a queue.');
      console.log(error);
    });
  }
};

const getCurrentDateTimeString = function () {
  const date = new Date();
  return date;
};

const sleep = async function (milliSeconds) {
  return new Promise(resolve => setTimeout(resolve, milliSeconds));
};

const getEndpointUrlByEntityTypeID = function (entityTypeID) {
  // Import ClickTimeAPI constats from App Constants
  const ClickTimeAPI = require('./app_constants').ClickTimeAPI;
  let endpointUrl = '';
  switch (entityTypeID) {
    case entityTypes.Users:
      endpointUrl = ClickTimeAPI.Endpoints.CreateUser;
      break;
    case entityTypes.Allocations:
      endpointUrl = ClickTimeAPI.Endpoints.CreateAllocation;
      break;
    case entityTypes.Jobs:
      endpointUrl = ClickTimeAPI.Endpoints.CreateJob;
      break;
    case entityTypes.Tasks:
      endpointUrl = ClickTimeAPI.Endpoints.CreateTask;
      break;
    case entityTypes.TimeEntries:
      endpointUrl = ClickTimeAPI.Endpoints.CreateTimeEntries;
      break;
    case entityTypes.Divisions:
      endpointUrl = ClickTimeAPI.Endpoints.CreateDivisions;
      break;
    case entityTypes.Clients:
      endpointUrl = ClickTimeAPI.Endpoints.CreateClients;
      break;
    default:
      break;
  }
  return endpointUrl;
};
