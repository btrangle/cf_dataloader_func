// Import getResponseObject method from general utilities module
const getResponseObject = require('../general_utilities').getResponseObject;
module.exports = async function (context, req) {
  await require('../general_utilities').validateUsersAuthenticity(req).then(async requestValidation => {
    if (requestValidation.ValidationStatus) {
      // Import ImportJobStatus constats from App Constants
      const ImportJobStatus = require('../app_constants').ImportJobStatus;
      let importJobStatus = null;
      let errorMessage = '';
      if(req.query.status) {
        switch(req.query.status) {
          case 'notstarted':
            importJobStatus = ImportJobStatus.NotStarted;
            break;
          case 'processing':
            importJobStatus = ImportJobStatus.InProcess;
            break;
          case 'complete':
            importJobStatus = ImportJobStatus.Completed;
            break;
          default:
            importJobStatus = 0;
        }
      }
      let importJobsListWithStatus = {};
      importJobsListWithStatus = await require('../DatabaseOperations/general_operations').getImportJobsListWithStatus({
        userID: requestValidation.UserID,
        importJobStatusID: importJobStatus
      }).catch(error => {
        errorMessage = error;
      });
      context.res = getResponseObject({
        status: errorMessage.length === 0 ? 200 : 400,
        body: errorMessage.length === 0 ? JSON.stringify(importJobsListWithStatus) : errorMessage
      });
    } else {
      context.res = getResponseObject({
        status: requestValidation.Status,
        body: requestValidation.ResponseMessage
      });
    }
  }).catch(requestValidation => {
    context.res = getResponseObject({
      status: requestValidation.Status,
      body: requestValidation.ResponseMessage
    });
  });
};
