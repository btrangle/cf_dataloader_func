const nodemailer = require('nodemailer');

// Import AzureFunctions constats from App Constants
const AzureFunctions = require('./app_constants').AzureFunctions;

const transporter = nodemailer.createTransport({
  host: process.env['SMTPHost'],
  port: process.env['SMTPPort'],
  secure: process.env['SSL'],
  auth: {
    user: process.env['SMTPEmailAddress'],
    pass: process.env['SMTPEmailPassword']
  }
});

/**
 * This Module provide the methods to Send Email
 */
module.exports = {
  /**
  * SendEmail
  * @param string recipientName
  * @param string recipientEmail
  * @param string importJobSummary
  */
  sendJobCompletionEmail: async function ({ recipientName, recipientEmail, importJobSummary }) {
    return new Promise(async (resolve, reject) => {
      try{
        const completeReportUrl = `${process.env['AzureInstanceBaseURL']}${AzureFunctions.GetImportJobDetail}?jobid=${importJobSummary.ImportJobID}`;
        const errorReportUrl = `${process.env['AzureInstanceBaseURL']}${AzureFunctions.GetImportJobDetail}?jobid=${importJobSummary.ImportJobID}&status=error`;
        const emailBody = `<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="m_2050175783579165704container" style="border-collapse:collapse;width:100%;min-width:100%;height:auto">
                <tbody><tr>
                  <td width="100%" valign="top" bgcolor="#ffffff" style="padding-top:20px">
                    <table width="640" class="m_2050175783579165704deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-collapse:collapse;margin:0 auto">
                      <tbody><tr>
                        <td valign="top" align="center" style="padding:0" bgcolor="#ffffff">
                          <a href="https://www.getpostman.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.getpostman.com/&amp;source=gmail&amp;ust=1578052069033000&amp;usg=AFQjCNFA-z5z7-9mW--yJMkdfFmR7PkYog">
                            <img class="m_2050175783579165704deviceWidth CToWUd" src="https://s9347.pcdn.co/images/clicktime-logo-blue.png" alt="" border="0" width="125" style="display:block">
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td style="font-size:13px;color:#282828;font-weight:normal;text-align:left;font-family:Roboto,'Open Sans',sans-serif;line-height:24px;vertical-align:top;padding:15px 8px 10px 8px" bgcolor="#ffffff">
                          <h1 style="text-align:center;font-weight:600;margin:30px 0 50px 0"><span class="il">IMPORT JOB COMPLETED</span></h1>
                          <p>Dear ${recipientName},</p>
                            <p>Your import job having number "${importJobSummary.ImportJobID}" has been completed. Below is the summary:</p>
                        </td>
                      </tr>
                      <tr>
                          <td style="padding-bottom:30px">
                            <table cellspacing="0" cellpadding="5" style="font-size:15px;color:#282828;font-weight:normal;text-align:left;font-family:Roboto,'Open Sans',sans-serif;line-height:24px;vertical-align:top;width:60%;margin: auto;" bgcolor="#ffffff">
                              <tbody>
                                <tr style="background-color:#016677;color:white" >
                                  <td>  Entity Type: </td>
                                  <td>&nbsp;&nbsp; ${importJobSummary.EntityType}</td>
                                </tr>
                                <tr style="background-color: #1b81d2;color:white" >
                                  <td>  Records Processed: </td>
                                  <td>&nbsp;&nbsp; ${importJobSummary.TotalRecords}</td>
                                </tr>
                                <tr style="background-color: green;color:white">
                                  <td>  Successfully Created:  </td>
                                  <td>&nbsp;&nbsp; ${importJobSummary.CreatedSuccessfully}</td>
                                </tr>
                                <tr style="background-color: red;color:white">
                                  <td>  Failed to Create: </td>
                                  <td>&nbsp;&nbsp; ${importJobSummary.FailedToCreat}</td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                      </tr>
                      <tr>
                          <td style="font-family:Roboto,'Open Sans',sans-serif;font-size:13px;padding:0px 10px 0px 10px;text-align:left;">                    
                            <p>You can get the detailed report using links below: </p>
                            <ul>
                              <li>
                                <a href="${completeReportUrl}" style="color:#2095f2;text-decoration:underline;font-weight:bold" target="_blank">Complete Report</a>
                              </li>
                              <li>
                                <a href="${errorReportUrl}" style="color:#2095f2;text-decoration:underline;font-weight:bold" target="_blank">Errors Report</a>
                              </li>
                            </ul>
                        </td>
                      </tr>
                      <tr>
                          <td style="font-family:Roboto,'Open Sans',sans-serif;font-size:13px;padding:0px 10px 0px 10px;text-align:left;">                    
                            <p>Cheers,<br>ClickTime Team</p>
                        </td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>`;
        await module.exports.sendEmail(`${importJobSummary.EntityType} Import Job Completion`, recipientEmail, emailBody, true);
        resolve();
      } catch(error) {
        reject(error);
      }
    });
  },
  /**
  * SendEmail
  * @param string emailSubject
  * @param string recipientEmail
  * @param string recipientEmail
  * @param boolean isHtml
  */
  sendEmail: async function (emailSubject, recipientEmail, emailBody, isHtml = false) {
    return new Promise((resolve, reject) => {
      try{
        const fromAddress = {
          name: process.env['SenderName'],
          address: process.env['SMTPEmailAddress']
        };
        var emailMessage = {
          from: fromAddress,
          to: recipientEmail,
          subject: emailSubject
        };
        if(isHtml) {
          emailMessage.html = emailBody;
        } else{
          emailMessage.text = emailBody;
        }

        transporter.sendMail(emailMessage, function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log(`Email sent: ${info.response}`);
          }
          resolve();
        });
      } catch(error) {
        reject(error);
      }
    });
  }
};
