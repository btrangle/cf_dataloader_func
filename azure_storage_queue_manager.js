// import azure storage SDk to use Azure Storage
const azure = require('azure-storage');
const queueSvc = azure.createQueueService(process.env['AzureStorageAccountConnectionString']);
// Import ImportJobStatus constats from App Constants
const ImportJobStatus = require('./app_constants').ImportJobStatus;
// Import Database Operations Module
const DatabaseGeneralOperations = require('./DatabaseOperations/general_operations');
// Import Module to Push requests to Azure Storage Queue
const ImportJobProcessor = require('./import_job_processor');

module.exports = {
  /**
     *
     * @param {string} queueName
     * @param {number} importJobID
     * @param {string} userEmail
     * @param {string} userName
     * @param {string} authToken
     */
  manageAzureStorageQueue: async function ({ queueName, importJobID, entityTypeID, userEmail, userName, authToken }) {
    if(queueName && queueName !== '' && importJobID && userEmail && authToken) {
      await handleImportJobRequest({
        queueName: queueName,
        importJobID: importJobID,
        entityTypeID: entityTypeID,
        userEmail: userEmail,
        userName: userName,
        authToken: authToken
      }).then(async jobHandlerStatus => {
        console.log(`jobHandlerStatus: ${jobHandlerStatus}`);
        await ImportJobProcessor.processImportJob(queueName).catch(error => {
          console.log('Exception occured in processImportJob method');
          console.log(error);
        });
      }).catch(error => {
        console.log('Exception occured in handleImportJobRequest method');
        console.log(error);
      });
    } else{
      await DatabaseGeneralOperations.getOldestImportJobs().then(async oldestImportJobs => {
        if(oldestImportJobs && oldestImportJobs.length > 0) {
          for (const importJob of oldestImportJobs) {
            const queueName = `${importJob.UserID.toLowerCase()}00${importJob.CompanyID.toLowerCase()}`;
            await handleImportJobRequest({
              queueName: queueName,
              importJobID: importJob.ImportJobID,
              entityTypeID: entityTypeID,
              userEmail: importJob.UserEmail,
              userName: importJob.UserName,
              authToken: importJob.AuthToken
            }).then(async jobHandlerStatus => {
              console.log(`jobHandlerStatus: ${jobHandlerStatus}`);
              await ImportJobProcessor.processImportJob(queueName).catch(error => {
                console.log('Exception occured in processImportJob method');
                console.log(error);
              });
            }).catch(error => {
              console.log('Exception occured in handleImportJobRequest method');
              console.log(error);
            });
          }
        }
      }).catch(error => {
        console.log('Exception occured in handleImportJobRequest method');
        console.log(error);
      });
    }
  },
  /**
     *
     * @param {string} queueName
     * @param {number} queueLength
     */
  getMessagesFromQueue: function ({ queueName, queueLength }) {
    const response = { Status: false, ErrorMessage: '' };
    return new Promise(async (resolve, reject) => {
      try{
        let batchLength = 0;
        if(queueLength === -1) {
          const queueLengthResponse = await module.exports.getQueueLength(queueName);
          if(queueLengthResponse.Status) {
            queueLength = queueLengthResponse.QueueLength;
          }
        }
        batchLength = queueLength > 32 ? 32 : queueLength;
        queueSvc.getMessages(queueName, { numOfMessages: batchLength }, function (error, results, getResponse) {
          if(!error) {
            resolve(results);
          }
          resolve(null);
        });
      } catch(ex) {
        response.ErrorMessage = ex.message;
        reject(response);
      }
    });
  },
  /**
     *
     * @param {string} queueName
     */
  getQueueLength: function (queueName) {
    const response = { Status: false, ErrorMessage: '', QueueLength: 0 };
    return new Promise((resolve, reject) => {
      try{
        queueSvc.getQueueMetadata(queueName, function (error, results, azureResponse) {
          if(!error) {
            // Queue length is available in results.approximateMessageCount
            response.Status = true;
            response.QueueLength = results.approximateMessageCount;
            resolve(response);
          } else{
            response.ErrorMessage = error.message;
            reject(response);
          }
        });
      } catch(ex) {
        response.ErrorMessage = ex.message;
        reject(response);
      }
    });
  },
  /**
     *
     * @param {string} queueName
     * @param {string} message
     * @param {string} messageText
     */
  updateMessageInQueue: function ({ queueName, message, messageText }) {
    const response = { Status: false, ErrorMessage: '' };
    return new Promise((resolve, reject) => {
      try{
        queueSvc.updateMessage(queueName, message.messageId, message.popReceipt, 10, { messageText: JSON.stringify(messageText) }, function (error, updateResults, updateResponse) {
          if(!error) {
            resolve(true);
          } else{
            resolve(error.message);
          }
        });
      } catch(ex) {
        response.ErrorMessage = ex.message;
        reject(response);
      }
    });
  },
  /**
     *
     * @param {string} queueName
     * @param {string} message
     */
  deleteMessageFromQueue: function ({ queueName, message }) {
    const response = { Status: false, ErrorMessage: '' };
    return new Promise((resolve, reject) => {
      try{
        queueSvc.deleteMessage(queueName, message.messageId, message.popReceipt, async function (error, deleteResponse) {
          if(!error) {
            // Message deleted
            response.Status = true;
          } else{
            response.ErrorMessage = error.message;
          }
          resolve(response);
        });
      } catch(ex) {
        response.ErrorMessage = ex.message;
        reject(response);
      }
    });
  },
  /**
     *
     * @param {string} queueName
     */
  deleteQueue: function (queueName) {
    const response = { Status: false, ErrorMessage: '' };
    return new Promise((resolve, reject) => {
      try{
        queueSvc.deleteQueue(queueName, function (error, response) {
          if(!error) {
            // Queue has been deleted
            response.Status = true;
            resolve(response);
          } else{
            response.ErrorMessage = error.message;
            reject(response);
          }
        });
      } catch(ex) {
        response.ErrorMessage = ex.message;
        reject(response);
      }
    });
  }
};

async function createQueue (queueName) {
  const response = { Status: false, ErrorMessage: '' };
  return new Promise((resolve, reject) => {
    try{
      queueSvc.createQueueIfNotExists(queueName, function (error, result, response) {
        if(!error) {
          // Queue created or exists
          result.created ? console.log('Queue Successfully Created.') : console.log('Queue Already Exist.');
          response.Status = true;
        } else{
          response.ErrorMessage = error;
        }
        resolve(response);
      });
    } catch(ex) {
      response.ErrorMessage = ex.message;
      reject(response);
    }
  });
};

function pushImportJobToQueue ({ queueName, entityTypeID, importJobID, userEmail, userName, authToken }) {
  const response = { Status: false, ErrorMessage: '' };
  return new Promise((resolve, reject) => {
    try{
      const messageData = JSON.stringify({ ImportJobID: importJobID, EntityTypeID: entityTypeID, UserName: userName, UserEmail: userEmail, AuthToken: authToken });
      queueSvc.createMessage(queueName, messageData, function (error, results, serviceResponse) {
        if(!error) {
          response.Status = true;
        } else{
          response.ErrorMessage = error;
        }
        resolve(response);
      });
    } catch(ex) {
      response.ErrorMessage = ex.message;
      reject(response);
    }
  });
};

async function handleImportJobRequest ({ queueName, importJobID, entityTypeID, userEmail, userName, authToken }) {
  return new Promise((resolve, reject) => {
    const response = { Status: false, ErrorMessage: '' };
    try{
      queueSvc.doesQueueExist(queueName, async function (error, result, azureResponse) {
        if(!error) {
          if(!result.exists) {
            // If queue dose not exist check for Number of users for which jobs are in process
            await DatabaseGeneralOperations.getInProcessJobCount().then(async inProcessJobCount => {
              const maximumNoOfAzureStorageQueues = require('./app_constants').MaximumNoOfAzureStorageQueues;
              if(inProcessJobCount < maximumNoOfAzureStorageQueues) {
                // Create queue if Number of users for which jobs are in process are less then limit define in config
                await createQueue(queueName);
              }
            });
          }
          // push Import Job to Queue
          await pushImportJobToQueue({
            queueName: queueName,
            importJobID: importJobID,
            entityTypeID: entityTypeID,
            userEmail: userEmail,
            userName: userName,
            authToken: authToken
          }).then(async messageCreationStatus => {
            // update job status in database once it's pushed to Azure Storage Queue
            if(messageCreationStatus.Status) {
              await DatabaseGeneralOperations.updateImportJobStatus({
                importJobID: importJobID,
                importJobStatusID: ImportJobStatus.PendingProcess
              }).catch(error => {
                console.error(error);
              });
              response.Status = true;
            } else{
              response.Status = false;
            }
          }).catch(error => {
            console.error(error);
          });
          resolve(response);
        } else{
          response.ErrorMessage = error.message;
          reject(response);
        }
      });
    } catch(ex) {
      response.ErrorMessage = ex.message;
      reject(response);
    }
  });
};
