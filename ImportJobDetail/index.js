// Import getResponseObject method from general utilities module
const getResponseObject = require('../general_utilities').getResponseObject;
module.exports = async function (context, req) {
  context.log('Function recieved request to get Import Job Detail.');
  const requestValidation = await validateRequest(req);
  if (requestValidation.ValidationStatus) {
    const requestParameters = req.query;
    // get entity id for job
    await require('../DatabaseOperations/general_operations').getEntityTypeIDByImportJobID(requestParameters.jobid).then(async entityTypeID => {
      let importJobData = {};
      let errorMessage = '';
      if(entityTypeID !== 0) {
        const getImportJobData = require('../DatabaseOperations/general_operations').getImportJobData;
        if (requestParameters.status && requestParameters.status.toLowerCase().trim() === 'error') {
          importJobData = await getImportJobData({
            importJobID: requestParameters.jobid,
            createdSuccessfully: 0,
            entityTypeID: entityTypeID
          }).catch(error => {
            errorMessage = error;
          });
        } else if (requestParameters.status && requestParameters.status.toLowerCase().trim() === 'success') {
          importJobData = await getImportJobData({
            importJobID: requestParameters.jobid,
            createdSuccessfully: 1,
            entityTypeID: entityTypeID
          }).catch(error => {
            errorMessage = error;
          });
        } else{
          importJobData = await getImportJobData({
            importJobID: requestParameters.jobid,
            isAllDataRequire: true,
            entityTypeID: entityTypeID
          }).catch(error => {
            errorMessage = error;
          });
        }
      }else {
        errorMessage = 'Invalid Job ID';
      }
      context.res = getResponseObject({
        status: errorMessage.length === 0 ? 200 : 400,
        body: errorMessage.length === 0 ? JSON.stringify(importJobData) : errorMessage
      });
    }).catch(error => {
      console.error(error);
      context.res = getResponseObject({
        status: 400,
        body: error
      });
    });
  } else {
    context.res = getResponseObject({
      status: requestValidation.ResponseCode,
      body: requestValidation.ResponseMessage
    });
  }
};

/**
 * Validate Http Request Parameters
 * @param HttpRequestObject HttpRequest Object
 * @return boolean true or false
 * IF request is valid return true
 */
const validateRequest = async function (req) {
  let requestValidation = { ValidationStatus: false, ResponseMessage: 'Invalid Request', ResponseCode: 400, UserID: null, CompanyID: null };
  try{
    if(req.query.jobid) {
      await require('../general_utilities').validateUsersAuthenticity(req).then(async userAuthenticityStatus => {
        requestValidation = userAuthenticityStatus;
      });
    } else{
      requestValidation.ValidationStatus = false;
      requestValidation.ResponseMessage = 'Missing parameter jobid';
      requestValidation.ResponseCode = 400;
    }
  } catch(ex) {
    requestValidation.ResponseMessage = ex.message;
  }
  return requestValidation;
};
